import express from 'express';
import config from 'config';
import http from 'http';
import socketIO from 'socket.io';

export const app = express();

const server = http.createServer(app); 

export const socketServer = socketIO(server, {
  path: '/ws/csp/socket.io',
  origins:'*:*',
  transports: 	['websocket', 'polling'],
  // pingInterval: 60000,
});

const socketNameSpace = config.get('socketNameSpace');
 
require('./startup/logging');
require('./startup/validation')();
require('./startup/db')(app);
require('./startup/esb');
require('./startup/routes')(app);
require('./startup/socket')(socketNameSpace ? socketServer.of(socketNameSpace) : socketServer);

require('./services/schedules');

// const server = app.listen(config.get('port'), () => {
//   console.log(`[${config.get('name')}] service running on port ${server.address().port}.`);
// });

server.listen(config.get('port'), () => {
  console.log(`[${config.get('name')}] service running on port ${server.address().port}.`, Date.now());
});
