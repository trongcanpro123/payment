import mongoose from 'mongoose';
import config from 'config';
import debug from 'debug';

require('../services/models');
const dbDebugger = debug('app:db');

module.exports = function(app) {
  mongoose.Promise = global.Promise;

  if (app.get('env') === "development") {
    mongoose.set('debug', true);
  }

  const db = config.get('database');

  mongoose.connect(db, { useNewUrlParser: true });

  mongoose.connection.on('connected', function () {
    dbDebugger(`Mongoose connected to ${db}`);
  });

  mongoose.connection.on('error',function (err) {  
    console.log(err);
  }); 
  
  mongoose.connection.on('disconnected', function () {  
    console.log('Mongoose disconnected'); 
  });
  
  process.on('SIGINT', function() {  
    mongoose.connection.close(function () { 
      process.exit(0); 
    }); 
  }); 
}
