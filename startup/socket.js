import mongoose from 'mongoose';
import debug from 'debug';
import jwt from 'jsonwebtoken';
import fs from 'fs';
import path from 'path';
import { app } from '../server';

const socketDebugger = debug('app:socket');

module.exports = function(io) {
  io.on('connection', socket => {
    socketDebugger(`[Socket] Socket ${ socket.id } is connected!`);
    // socket.emit('newMessage', 'Hi!');

    socket.auth = false;    

    socket.on('authenticate', (data) => {
      const token = data.token; // ko dung toan tu {} vi bi loi ko lay duoc token
      // socketDebugger(`[Socket] Received token: ${ token }`);

      if (token) {
        const cert = fs.readFileSync(path.resolve(__dirname, '../certs/token.public.pem'));
    
        jwt.verify(token, cert, function(err, jwtPayload) {
          if (err) {
            socketDebugger(`[Socket] Socket ${ socket.id } has invalid token.`);
            socket.disconnect('Unauthorized');
          } else {
            socketDebugger(`[Socket] Socket ${ socket.id } is authorized!`);
            socket.auth = true;
            socket.userId = jwtPayload['userId'] ? jwtPayload['userId'] : '';
            socket.userName = jwtPayload['userName'] ? jwtPayload['userName'] : '';

            app.locals.socket = socket;

            const RequestedDataModel = mongoose.model('comments');
            
            RequestedDataModel.find({})
              .select({ _id: 1, subject: 1, refUrl: 1, content: 1 })
              // .limit(20)
              .sort({ createdAt: -1 })
              .exec( (err, msgList) => {
                if (!err) {
                  socket.emit(`msg2`, msgList);
                }
              });
          }
        });
      }
    });

    setTimeout(() => {
      if (!socket.auth) {
        socketDebugger(`[Socket] Force socket ${ socket.id } to disconnect...`);
        socket.disconnect('Unauthorized');
      }
    }, 1000);
    
    socket.on('disconnect', () => {
      socketDebugger(`[Socket] Socket ${ socket.id } disconnected!`);
    })
  })
}
