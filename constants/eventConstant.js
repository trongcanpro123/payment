export const commonEvent = {};


commonEvent.CREATED = 'created';
commonEvent.CREATED_BY_API = 'createdByApi';
commonEvent.CREATED_BY_API_V2 = 'createdByApiV2';
commonEvent.CREATE_UOM = 'create_uom';
commonEvent.CREATE_IMPORT = 'create_import';
commonEvent.UPDATED = 'updated';
commonEvent.DELETE_IN_PROCESS = 'deleteInProcess';
commonEvent.DELETE_REJECTED = 'deleteRejected';
commonEvent.DELETED = 'deleted';
commonEvent.COMBINE_PURCHASE = 'combinePurchase';
commonEvent.UPLOAD_TENANTID = 'uploadTenantID';
commonEvent.DONE = 'done';
commonEvent.PROCESS = 'process';
commonEvent.WAIT_LICENSE = 'waitLicense';
commonEvent.UPDATED_DOMAIN = 'updateDomain';
commonEvent.SENT = 'sent';


commonEvent.HANDLED = 'handled';
commonEvent.CAN_NOT_HANDLED = 'canNotHandled';

commonEvent.CREATE_DOMAIN_DONE = "createTenantDomainDone";
commonEvent.CREATE_DOMAIN_ERROR = "createTenantDomainError";
commonEvent.AGREEMENT_DOMAIN_DONE = "agreementTenantDomainDone";
commonEvent.AGREEMENT_DOMAIN_ERROR = "agreementTenantDomainError";

export default commonEvent;
