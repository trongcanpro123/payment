export const HTTP_METHODS = {
  values: ['get', 'post', 'put', 'delete'],
  message: 'enum validator failed for path `{PATH}` with value `{VALUE}`',
};

export const HTTP_RESPONSE_CODE = {
  OK: '200',
  CREATED: '201',
  MOVED_PERMANENTLY: '301',
  UNAUTHORIZED: '401',
  FORBIDDEN: '403',
  NOT_FOUND: '404',
  METHOD_NOT_ALLOWED: '405',
  UNPROCESSABLE_ENTITY: '422',
  INTERNAL_SERVER_ERROR: '500',
  BAD_GATEWAY: '500',
  SERVICE_UNAVAILABLE: '503'
}