export const DATA_TYPE = {
  ID: 'id',
  STRING: 'string',
  NUMBER: 'number',
  EMAIL: 'email',
  PHONE: 'phone',
  ARRAY: 'array',
  BOOLEAN: 'boolean',
  DATE: 'date',
  DATE_TIME: 'dateTime',
  OBJECT: 'object',
};

const EQ = '$eq';
const NEQ = '$neq';

const GT = '$gt';
const GTE = '$gte';

const LT = '$lt';
const LTE = '$lte';

const IN = '$in';
const NOT_IN = '$nin';
const EXISTS = '$exists';

// const EXITS_INTERSECTION = '$exits.intersection';
// const NOT_EXITS_INTERSECTION = '$not.exits.intersection';

export const OPERATOR_LIST = [
  EQ, NEQ,
  GT, GTE,
  LT, LTE,
  IN, NOT_IN, EXISTS
];

export const OPERATOR = {};

OPERATOR.EQ = EQ;
OPERATOR.NEQ = NEQ;

OPERATOR.GT = GT;
OPERATOR.GTE = GTE;

OPERATOR.LT = LT;
OPERATOR.LTE = LTE;

OPERATOR.IN = IN;
OPERATOR.NOT_IN = NOT_IN;
OPERATOR.EXISTS = EXISTS;

// OPERATOR.EXITS_INTERSECTION = EXITS_INTERSECTION;
// OPERATOR.NOT_EXITS_INTERSECTION = NOT_EXITS_INTERSECTION;