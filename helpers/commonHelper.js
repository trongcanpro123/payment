import mongoose from 'mongoose';
import { Client } from 'pg';
const nodemailer = require('nodemailer');
export const TOKEN = {};

TOKEN.YEAR = '{YY}';
TOKEN.MONTH = '{MM}';
TOKEN.DAY = '{DD}';
TOKEN.SEPARATOR = '{SEPARATOR}';
TOKEN.SEQUENCE = '{SEQUENCE}';

export const smtpMail = {};

smtpMail.HOST = 'mail.ftg.com.vn';
smtpMail.PORT = 587;
smtpMail.SECURE = false;
smtpMail.REQUIRETLS = true;
smtpMail.USER = 'noreply@ftg.com.vn';
smtpMail.PASS = 'fim@FTG123';
smtpMail.FROM = 'Synnex FPT <noreply@ftg.com.vn>';

export const validateObjectId = id => ((typeof id !== 'undefined') && (id !== null) && (id !== '') && (id !== '0'));

export const getDataCCMController = async (sql) => {

  // const client = new Client(process.env.DATABASE_CCM);
  const client = new Client('postgresql://ftgccm:ft6ccm@2O17@10.7.0.187/ftgccm');
  await client.connect();
  const query = await client.query(sql);
  await client.end();

  return query.rows;
};

export const sendEmail = (res, subject, content, sendTo, ccTo) => {
  let transporter = nodemailer.createTransport({
    host: smtpMail.HOST,
    port: smtpMail.PORT,
    secure: smtpMail.SECURE,
    requireTLS: smtpMail.REQUIRETLS,
    auth: {
      user: smtpMail.USER,
      pass: smtpMail.PASS
    },
    tls: {
      rejectUnauthorized: false
    }
  });

  let mailOptions = {
    from: smtpMail.FROM,
    to: String(sendTo),
    cc: ccTo + ',noreply@ftg.com.vn',
    subject: subject,
    html: content
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return res.status(500).json({ error });
    } else {
      return res.json({ msg: 'Sent to email' });
    }
  });
}

export const executePostgresqlDB = async (connectionString, sqlStatement) => {
  const client = new Client(connectionString);
  await client.connect();
  const query = await client.query(sqlStatement);
  await client.end();

  return query.rows;
}

export const getSequenceCode = async (codePattern, sequenceLength = 3, separator = '-') => {
  const Sequences = mongoose.model('sysSequences');
  let sequenceModel = codePattern;
  const now = new Date();
  const replacedToken = [];

  const year = now.getFullYear() % 100;

  if (year < 10) {
    replacedToken.push({ key: TOKEN.YEAR, value: `0${year}` });
  } else {
    replacedToken.push({ key: TOKEN.YEAR, value: `${year}` });
  }

  const month = now.getMonth() + 1;

  if (month < 10) {
    replacedToken.push({ key: TOKEN.MONTH, value: `0${month}` });
  } else {
    replacedToken.push({ key: TOKEN.MONTH, value: `${month}` });
  }

  const day = now.getDate();

  if (day < 10) {
    replacedToken.push({ key: TOKEN.DAY, value: `0${day}` });
  } else {
    replacedToken.push({ key: TOKEN.DAY, value: `${day}` });
  }

  replacedToken.push({ key: TOKEN.SEPARATOR, value: separator });
  console.log('replacedToken', replacedToken);
  replacedToken.forEach((token) => { // replace all supported token
    while (sequenceModel.indexOf(token.key) > -1) {
      sequenceModel = sequenceModel.replace(token.key, token.value);
    }
  });

  const codeTemplate = sequenceModel;
  sequenceModel = sequenceModel.replace(TOKEN.SEQUENCE, ''); // get unique key pattern

  let sequenceValue;
  await Sequences.findOne({ model: sequenceModel })
    .select('nextValue')
    .then((sequence) => {
      if (!sequence) {
        sequenceValue = '1';
        const newSequence = new Sequences({ model: sequenceModel, nextValue: 2 });
        newSequence.save();
      } else {
        sequenceValue = sequence.nextValue.toString();
        sequence.nextValue += 1;
        sequence.save();
      }
    });

  while (sequenceValue.length < sequenceLength) {
    sequenceValue = `0${sequenceValue}`;
  }
  return codeTemplate.replace(TOKEN.SEQUENCE, sequenceValue);
};

export const getAutoIncreaseCode = async (codePattern) => {
  const Sequences = mongoose.model('sysSequences');
  let sequenceModel = codePattern;
  // const replacedToken = [];

  // replacedToken.push({ key: TOKEN.SEPARATOR, value: separator });
  // console.log('replacedToken', replacedToken);
  // replacedToken.forEach((token) => { // replace all supported token
  //   while (sequenceModel.indexOf(token.key) > -1) {
  //     sequenceModel = sequenceModel.replace(token.key, token.value);
  //   }
  // });

  // sequenceModel = sequenceModel.replace(TOKEN.SEQUENCE, ''); // get unique key pattern

  let sequenceValue;
  await Sequences.findOne({ model: sequenceModel })
    .select('nextValue')
    .then((sequence) => {
      if (!sequence) {
        sequenceValue = '1';
        const newSequence = new Sequences({ model: sequenceModel, nextValue: 2 });
        newSequence.save();
      } else {
        sequenceValue = sequence.nextValue.toString();
        sequence.nextValue += 1;
        sequence.save();
      }
    });

  return sequenceValue;
};


export const getSequenceMarketingCode = async (codePattern, sequenceLength = 3, separator = '-') => {
  const Sequences = mongoose.model('sequences');
  let sequenceModel = codePattern;
  const now = new Date();
  const replacedToken = [];

  const year = now.getFullYear() ;

  if (year < 10) {
    replacedToken.push({ key: TOKEN.YEAR, value: `0${year}` });
  } else {
    replacedToken.push({ key: TOKEN.YEAR, value: `${year}` });
  }

  const month = now.getMonth() + 1;

  if (month < 10) {
    replacedToken.push({ key: TOKEN.MONTH, value: `0${month}` });
  } else {
    replacedToken.push({ key: TOKEN.MONTH, value: `${month}` });
  }

  const day = now.getDate();

  if (day < 10) {
    replacedToken.push({ key: TOKEN.DAY, value: `0${day}` });
  } else {
    replacedToken.push({ key: TOKEN.DAY, value: `${day}` });
  }

  replacedToken.push({ key: TOKEN.SEPARATOR, value: separator });

  replacedToken.forEach((token) => { // replace all supported token
    while (sequenceModel.indexOf(token.key) > -1) {
      sequenceModel = sequenceModel.replace(token.key, token.value);
    }
  });

  const codeTemplate = sequenceModel;
  sequenceModel = sequenceModel.replace(TOKEN.SEQUENCE, ''); // get unique key pattern

  let sequenceValue;
  await Sequences.findOne({ model: sequenceModel })
    .select('nextValue')
    .then((sequence) => {
      if (!sequence) {
        sequenceValue = '1';
        const newSequence = new Sequences({ model: sequenceModel, nextValue: 2 });
        newSequence.save();
      } else {
        sequenceValue = sequence.nextValue.toString();
        sequence.nextValue += 1;
        sequence.save();
      }
    });

  while (sequenceValue.length < sequenceLength) {
    sequenceValue = `0${sequenceValue}`;
  }

  return codeTemplate.replace(TOKEN.SEQUENCE, sequenceValue);
};

export const checkDuplicateAccountingCodeCustomer = async (customerCode, accountingcode) => {
  try {
    const ProductModel = mongoose.model('customers');
    return new Promise(function (resolve, reject) {
      ProductModel.find({ accountingCode: accountingcode }).exec(function (err, branchs) {
        if (err) {
          throw err;
        }
        else {
          if (branchs.length === 0) {
            resolve(accountingcode);
          }
          else {
            if (branchs[0].customerCode === customerCode) {
              resolve(accountingcode);
            }
            else {
              resolve(false);
            }
          }
        };
      })
    }
    );
  }
  catch (error) {
    throw new Error('checkDuplicateAccountingCodeCustomer Function Error');
  }
};

export const getSequenceFundCode = async (codePattern, sequenceLength = 3, separator = '-') => {
  const Sequences = mongoose.model('sequences');
  let sequenceModel = codePattern;
  const now = new Date();
  const replacedToken = [];

  const year = now.getFullYear() ;

  if (year < 10) {
    replacedToken.push({ key: TOKEN.YEAR, value: `0${year}` });
  } else {
    replacedToken.push({ key: TOKEN.YEAR, value: `${year}` });
  }

  const month = now.getMonth() + 1;

  if (month < 10) {
    replacedToken.push({ key: TOKEN.MONTH, value: `0${month}` });
  } else {
    replacedToken.push({ key: TOKEN.MONTH, value: `${month}` });
  }

  const day = now.getDate();

  if (day < 10) {
    replacedToken.push({ key: TOKEN.DAY, value: `0${day}` });
  } else {
    replacedToken.push({ key: TOKEN.DAY, value: `${day}` });
  }

  replacedToken.push({ key: TOKEN.SEPARATOR, value: separator });

  replacedToken.forEach((token) => { // replace all supported token
    while (sequenceModel.indexOf(token.key) > -1) {
      sequenceModel = sequenceModel.replace(token.key, token.value);
    }
  });

  const codeTemplate = sequenceModel;
  sequenceModel = sequenceModel.replace(TOKEN.SEQUENCE, ''); // get unique key pattern

  let sequenceValue;
  await Sequences.findOne({ model: sequenceModel })
    .select('nextValue')
    .then((sequence) => {
      if (!sequence) {
        sequenceValue = '1';
        const newSequence = new Sequences({ model: sequenceModel, nextValue: 2 });
        newSequence.save();
      } else {
        sequenceValue = sequence.nextValue.toString();
        sequence.nextValue += 1;
        sequence.save();
      }
    });

  while (sequenceValue.length < sequenceLength) {
    sequenceValue = `0${sequenceValue}`;
  }

  return codeTemplate.replace(TOKEN.SEQUENCE, sequenceValue);
};


export const equalToId = (id, otherId) => {
  if (!id || !otherId) {
    return false;
  }

  return (id.toString() === otherId.toString());
};

export const containId = (objectList, lookUpId) => {
  if (_.isArray(objectList) && lookUpId) {
    return objectList.findIndex(f => equalToId(f, lookUpId)) > -1;
  }

  return false;
};

export const equalToDate = (d1, d2) => {
  const year1 = d1.getFullYear();
  const month1 = d1.getMonth();
  const day1 = d1.getDate();

  const year2 = d2.getFullYear();
  const month2 = d2.getMonth();
  const day2 = d2.getDate();

  return ((year1 === year2) && (month1 === month2) && (day1 === day2));
}

export const greatThanDate = (d1, d2) => {
  const year1 = d1.getFullYear();
  const month1 = d1.getMonth();
  const day1 = d1.getDate();

  const year2 = d2.getFullYear();
  const month2 = d2.getMonth();
  const day2 = d2.getDate();

  if (year1 > year2) {
    return true;
  } 
  
  if (year1 < year2) {
    return false;
  }

  if (month1 > month2) {
    return true;
  }
  
  if (month1 < month2) {
    return false;
  }
  
  if (day1 > day2) {
    return true;
  } 
  
  if (day1 < day2) {
    return false;
  }
  
  return false;
}

export const greatThanOrEqualToDate = (d1, d2) => {
  const year1 = d1.getFullYear();
  const month1 = d1.getMonth();
  const day1 = d1.getDate();

  const year2 = d2.getFullYear();
  const month2 = d2.getMonth();
  const day2 = d2.getDate();

  if (year1 > year2) {
    return true;
  } 
  
  if (year1 < year2) {
    return false;
  }

  if (month1 > month2) {
    return true;
  }
  
  if (month1 < month2) {
    return false;
  }
  
  if (day1 > day2) {
    return true;
  } 
  
  if (day1 < day2) {
    return false;
  }
  
  return true;
}

export const lessThanDate = (d1, d2) => {
  const year1 = d1.getFullYear();
  const month1 = d1.getMonth();
  const day1 = d1.getDate();

  const year2 = d2.getFullYear();
  const month2 = d2.getMonth();
  const day2 = d2.getDate();

  if (year1 < year2) {
    return true;
  }
  
  if (year1 > year2) {
    return false;
  } 
  
  if (month1 < month2) {
    return true;
  }
  
  if (month1 > month2) {
    return false;
  }
  
  if (day1 < day2) {
      return true;
  }
  
  if (day1 > day2) {
    return false;
  }
  
  return false;
}

export const lessThanOrEqualToDate = (d1, d2) => {
  const year1 = d1.getFullYear();
  const month1 = d1.getMonth();
  const day1 = d1.getDate();

  const year2 = d2.getFullYear();
  const month2 = d2.getMonth();
  const day2 = d2.getDate();

  if (year1 < year2) {
    return true;
  }
  
  if (year1 > year2) {
    return false;
  } 
  
  if (month1 < month2) {
    return true;
  }
  
  if (month1 > month2) {
    return false;
  }
  
  if (day1 < day2) {
      return true;
  }
  
  if (day1 > day2) {
    return false;
  }
  
  return true;
}

export const equalToDateTime = (d1, d2) => {
  const year1 = d1.getFullYear();
  const mon1 = d1.getMonth();
  const day1 = d1.getDate();
  const hour1 = d1.getHours();
  const min1 = d1.getMinutes();
  const sec1 = d1.getSeconds();


  const year2 = d2.getFullYear();
  const mon2 = d2.getMonth();
  const day2 = d2.getDate();
  const hour2 = d2.getHours();
  const min2 = d2.getMinutes();
  const sec2 = d2.getSeconds();

  return (
    (year1 === year2) && 
    (mon1 === mon2) && 
    (day1 === day2) && 
    (hour1 === hour2) && 
    (min1 === min2) && 
    (sec1 === sec2)
  );
}


export const greatThanDateTime = (d1, d2) => {
  const year1 = d1.getFullYear();
  const month1 = d1.getMonth();
  const day1 = d1.getDate();
  const hour1 = d1.getHours();
  const min1 = d1.getMinutes();
  const sec1 = d1.getSeconds();

  const year2 = d2.getFullYear();
  const month2 = d2.getMonth();
  const day2 = d2.getDate();
  const hour2 = d2.getHours();
  const min2 = d2.getMinutes();
  const sec2 = d2.getSeconds();

  if (year1 > year2) {
    return true;
  } 
  
  if (year1 < year2) {
    return false;
  }

  if (month1 > month2) {
    return true;
  }
  
  if (month1 < month2) {
    return false;
  }
  
  if (day1 > day2) {
    return true;
  } 
  
  if (day1 < day2) {
    return false;
  }

  if (hour1 > hour2) {
    return true;
  }
  
  if (hour1 < hour2) {
    return false;
  }

  if (min1 > min2) {
    return true;
  }
  
  if (min1 < min2) {
    return false;
  }

  if (sec1 > sec2) {
    return true;
  }
  
  if (sec1 < sec2) {
    return false;
  }
  
  return false;
}

export const greatThanOrEqualToDateTime = (d1, d2) => {
  const year1 = d1.getFullYear();
  const month1 = d1.getMonth();
  const day1 = d1.getDate();
  const hour1 = d1.getHours();
  const min1 = d1.getMinutes();
  const sec1 = d1.getSeconds();

  const year2 = d2.getFullYear();
  const month2 = d2.getMonth();
  const day2 = d2.getDate();
  const hour2 = d2.getHours();
  const min2 = d2.getMinutes();
  const sec2 = d2.getSeconds();

  if (year1 > year2) {
    return true;
  } 
  
  if (year1 < year2) {
    return false;
  }

  if (month1 > month2) {
    return true;
  }
  
  if (month1 < month2) {
    return false;
  }
  
  if (day1 > day2) {
    return true;
  } 
  
  if (day1 < day2) {
    return false;
  }

  if (hour1 > hour2) {
    return true;
  }
  
  if (hour1 < hour2) {
    return false;
  }

  if (min1 > min2) {
    return true;
  }
  
  if (min1 < min2) {
    return false;
  }

  if (sec1 > sec2) {
    return true;
  }
  
  if (sec1 < sec2) {
    return false;
  }
  
  return true;
}


export const lessThanDateTime = (d1, d2) => {
  const year1 = d1.getFullYear();
  const month1 = d1.getMonth();
  const day1 = d1.getDate();
  const hour1 = d1.getHours();
  const min1 = d1.getMinutes();
  const sec1 = d1.getSeconds();

  const year2 = d2.getFullYear();
  const month2 = d2.getMonth();
  const day2 = d2.getDate();
  const hour2 = d2.getHours();
  const min2 = d2.getMinutes();
  const sec2 = d2.getSeconds();

  if (year1 < year2) {
    return true;
  } 
  
  if (year1 > year2) {
    return false;
  }

  if (month1 < month2) {
    return true;
  }
  
  if (month1 > month2) {
    return false;
  }
  
  if (day1 < day2) {
    return true;
  } 
  
  if (day1 > day2) {
    return false;
  }

  if (hour1 < hour2) {
    return true;
  }
  
  if (hour1 > hour2) {
    return false;
  }

  if (min1 < min2) {
    return true;
  }
  
  if (min1 > min2) {
    return false;
  }

  if (sec1 < sec2) {
    return true;
  }
  
  if (sec1 > sec2) {
    return false;
  }
  
  return false;
}

export const lessThanOrEqualToDateTime = (d1, d2) => {
  const year1 = d1.getFullYear();
  const month1 = d1.getMonth();
  const day1 = d1.getDate();
  const hour1 = d1.getHours();
  const min1 = d1.getMinutes();
  const sec1 = d1.getSeconds();

  const year2 = d2.getFullYear();
  const month2 = d2.getMonth();
  const day2 = d2.getDate();
  const hour2 = d2.getHours();
  const min2 = d2.getMinutes();
  const sec2 = d2.getSeconds();

  if (year1 < year2) {
    return true;
  } 
  
  if (year1 > year2) {
    return false;
  }

  if (month1 < month2) {
    return true;
  }
  
  if (month1 > month2) {
    return false;
  }
  
  if (day1 < day2) {
    return true;
  } 
  
  if (day1 > day2) {
    return false;
  }

  if (hour1 < hour2) {
    return true;
  }
  
  if (hour1 > hour2) {
    return false;
  }

  if (min1 < min2) {
    return true;
  }
  
  if (min1 > min2) {
    return false;
  }

  if (sec1 < sec2) {
    return true;
  }
  
  if (sec1 > sec2) {
    return false;
  }
  
  return true;
}
export const toStringFormat = (dateTime, format) => { 
  format = format.toLowerCase();
  format = format.replace('dd', (((dateTime.getDate() < 10) ? "0" : "") + dateTime.getDate()));
  format = format.replace('mm', (((dateTime.getMonth() < 9) ? "0" : "") + (dateTime.getMonth() + 1)));
  format = format.replace('yyyy', dateTime.getFullYear());
  format = format.replace('hh', (((dateTime.getHours() < 10) ? "0" : "") + dateTime.getHours()));
  format = format.replace('mm', (((dateTime.getMinutes() < 10) ? "0" : "") + dateTime.getMinutes()));
  format = format.replace('ss', (((dateTime.getSeconds() < 10) ? "0" : "") + dateTime.getSeconds()));
  
  return format;
}