import _ from 'lodash';

import {
  exactDepartmentIdList,
  exactCompanyIdList,
  exactRoleIdList,
  exactStaffIdList,
  exactBossIdList,
  getUserFeature,
  getUserAccessList,
  checkFieldPermission,
  checkRecordPermission,
} from './policyHelper.js';

import { TITLE } from '../constants/titleConstant';
import { USER_FEATURE } from '../constants/policyConstant';
import { DATA_TYPE, OPERATOR } from '../constants/dataTypeConstant';

describe('Test exactDepartmentIdList', () => {

  test('exactDepartmentIdList is defined', () => {
    expect(exactDepartmentIdList).toBeDefined();
  });

  test('user is null', () => {
    const departmentList = exactDepartmentIdList(null);

    expect(_.isArray(departmentList)).toBeTruthy();
    expect(departmentList).toHaveLength(0);
  });

  test('user has NOT departmentList', () => {
    const departmentList = exactDepartmentIdList({});

    expect(_.isArray(departmentList)).toBeTruthy();
    expect(departmentList).toHaveLength(0);
  });

  test('user has departmentList', () => {
    const departmentList = exactDepartmentIdList({
      departmentList: [
        {
          departmentId: 'id#1',
          departmentCode: 'code#1',
          departmentName: 'name#1',
        },
        {
          departmentId: 'id#2',
          departmentCode: 'code#2',
          departmentName: 'name#2',
        },
        {
          departmentId: 'id#3',
          departmentCode: 'code#3',
          departmentName: 'name#3',
        },
      ]
    });


    expect(_.isArray(departmentList)).toBeTruthy();
    expect(departmentList).toEqual(expect.arrayContaining(['id#1', 'id#2', 'id#3']));
  });
});

describe('Test exactCompanyIdList', () => {

  test('exactCompanyIdList is defined', () => {
    expect(exactCompanyIdList).toBeDefined();
  });

  test('user is null', () => {
    const companyList = exactCompanyIdList(null);

    expect(_.isArray(companyList)).toBeTruthy();
    expect(companyList).toHaveLength(0);
  });

  test('user has NOT departmentList', () => {
    const companyList = exactCompanyIdList({});

    expect(_.isArray(companyList)).toBeTruthy();
    expect(companyList).toHaveLength(0);
  });

  test('user has departmentList', () => {
    const companyList = exactCompanyIdList({
      departmentList: [
        {
          companyId: 'id#1',
          companyCode: 'code#1',
          companyName: 'name#1',
        },
        {
          companyId: 'id#2',
          companyCode: 'code#2',
          companyName: 'name#2',
        },
        {
          companyId: 'id#3',
          companyCode: 'code#3',
          companyName: 'name#3',
        },
      ]
    });

    expect(_.isArray(companyList)).toBeTruthy();
    expect(companyList).toEqual(expect.arrayContaining(['id#1', 'id#2', 'id#3']));
  });
});

describe('Test exactRoleIdList', () => {

  test('exactRoleIdList is defined', () => {
    expect(exactRoleIdList).toBeDefined();
  });

  test('user is null', () => {
    const roleList = exactRoleIdList(null);

    expect(roleList).toBeDefined();
    expect(_.isArray(roleList)).toBeTruthy();
    expect(roleList).toHaveLength(0);
  });

  test('user has NOT roleList', () => {
    const roleList = exactRoleIdList({});

    expect(_.isArray(roleList)).toBeTruthy();
    expect(roleList).toHaveLength(0);
  });

  test('user has roleList', () => {
    const roleList = exactRoleIdList({
      roleList: [
        {
          roleId: 'id#1',
          roleCode: 'code#1',
          roleName: 'name#1',
        },
        {
          roleId: 'id#2',
          roleCode: 'code#2',
          roleName: 'name#2',
        },
        {
          roleId: 'id#3',
          roleCode: 'code#3',
          roleName: 'name#3',
        },
      ]
    });

    expect(_.isArray(roleList)).toBeTruthy();
    expect(roleList).toEqual(expect.arrayContaining(['id#1', 'id#2', , 'id#3']));
  });
});

describe('Test exactStaffIdList', () => {

  test('exactStaffIdList is defined', () => {
    expect(exactStaffIdList).toBeDefined();
  });

  test('user is null', () => {
    const staffList = exactStaffIdList(null);

    expect(staffList).toBeDefined();
    expect(_.isArray(staffList)).toBeTruthy();
  });

  test('user has NOT departmentList', () => {
    const staffList = exactStaffIdList({});

    expect(_.isArray(staffList)).toBeTruthy();
    expect(staffList).toHaveLength(0);
  });

  test('user has NOT departmentList.userList', () => {
    const staffList = exactStaffIdList({
      departmentList: [
        {
          departmentId: 'id#1',
          departmentCode: 'code#1',
          departmentName: 'name#1',
        },
        {
          departmentId: 'id#2',
          departmentCode: 'code#2',
          departmentName: 'name#2',
        },
        {
          departmentId: 'id#3',
          departmentCode: 'code#3',
          departmentName: 'name#4',
        },
      ]
    });

    expect(_.isArray(staffList)).toBeTruthy();
    expect(staffList).toHaveLength(0);
  });

  test('user has departmentList.userList', () => {
    const staffList = exactStaffIdList({
      departmentList: [
        {
          departmentId: 'id#1',
          departmentCode: 'code#1',
          departmentName: 'name#1',
          userList: [
            {
              titleId: TITLE.STAFF,
              titleCode: 'titleCode#1',
              titleName: 'titleName#1',
  
              userId: 'userId#1',
              userName: 'userName#1',
              fullName: 'fullName#1',
              email: 'fullName#1',
              phone: 'phone#1'
            },

            {
              titleId: TITLE.STAFF,
              titleCode: 'titleCode#2',
              titleName: 'titleName#2',
  
              userId: 'userId#2',
              userName: 'userName#2',
              fullName: 'fullName#2',
              email: 'fullName#2',
              phone: 'phone#2'
            }
          ],
        },

        {
          departmentId: 'id#2',
          departmentCode: 'code#2',
          departmentName: 'name#2',
          userList: [
            {
              titleId: TITLE.STAFF,
              titleCode: 'titleCode#1',
              titleName: 'titleName#1',
  
              userId: 'userId#3',
              userName: 'userName#3',
              fullName: 'fullName#3',
              email: 'fullName#3',
              phone: 'phone#3'
            },

            {
              titleId: TITLE.CTO,
              titleCode: 'titleCode#2',
              titleName: 'titleName#2',
  
              userId: 'userId#4',
              userName: 'userName#4',
              fullName: 'fullName#4',
              email: 'fullName#4',
              phone: 'phone#4'
            }
          ],
        },

        {
          departmentId: 'id#3',
          departmentCode: 'code#3',
          departmentName: 'name#4',
          userList: [],
        },
      ]
    });

    expect(_.isArray(staffList)).toBeTruthy();
    expect(staffList).toEqual(expect.arrayContaining(['userId#1', 'userId#2', , 'userId#3']));
  });
});

describe('Test exactBossIdList', () => {

  test('exactBossIdList is defined', () => {
    expect(exactBossIdList).toBeDefined();
  });

  test('user is null', () => {
    const staffList = exactBossIdList(null);

    expect(staffList).toBeDefined();
    expect(_.isArray(staffList)).toBeTruthy();
  });

  test('user has NOT departmentList', () => {
    const staffList = exactBossIdList({});

    expect(staffList).toBeDefined();
    expect(_.isArray(staffList)).toBeTruthy();
    expect(staffList).toHaveLength(0);
  });

  test('user has NOT departmentList.userList', () => {
    const staffList = exactBossIdList({
      departmentList: [
        {
          departmentId: 'id#1',
          departmentCode: 'code#1',
          departmentName: 'name#1',
        },
        {
          departmentId: 'id#2',
          departmentCode: 'code#2',
          departmentName: 'name#2',
        },
        {
          departmentId: 'id#3',
          departmentCode: 'code#3',
          departmentName: 'name#4',
        },
      ]
    });

    expect(staffList).toBeDefined();
    expect(_.isArray(staffList)).toBeTruthy();
    expect(staffList).toHaveLength(0);
  });

  test('user has departmentList.userList', () => {
    const bossList = exactBossIdList({
      departmentList: [
        {
          departmentId: 'id#1',
          departmentCode: 'code#1',
          departmentName: 'name#1',
          userList: [
            {
              titleId: TITLE.STAFF,
              titleCode: 'titleCode#1',
              titleName: 'titleName#1',
  
              userId: 'userId#1',
              userName: 'userName#1',
              fullName: 'fullName#1',
              email: 'fullName#1',
              phone: 'phone#1'
            },

            {
              titleId: TITLE.STAFF,
              titleCode: 'titleCode#2',
              titleName: 'titleName#2',
  
              userId: 'userId#2',
              userName: 'userName#2',
              fullName: 'fullName#2',
              email: 'fullName#2',
              phone: 'phone#2'
            }
          ],
        },

        {
          departmentId: 'id#2',
          departmentCode: 'code#2',
          departmentName: 'name#2',
          userList: [
            {
              titleId: TITLE.STAFF,
              titleCode: 'titleCode#1',
              titleName: 'titleName#1',
  
              userId: 'userId#3',
              userName: 'userName#3',
              fullName: 'fullName#3',
              email: 'fullName#3',
              phone: 'phone#3'
            },

            {
              titleId: TITLE.CTO,
              titleCode: 'titleCode#2',
              titleName: 'titleName#2',
  
              userId: 'userId#4',
              userName: 'userName#4',
              fullName: 'fullName#4',
              email: 'fullName#4',
              phone: 'phone#4'
            }
          ],
        },

        {
          departmentId: 'id#3',
          departmentCode: 'code#3',
          departmentName: 'name#4',
          userList: [],
        },
      ]
    });

    expect(bossList).toBeDefined();
    expect(_.isArray(bossList)).toBeTruthy();
    expect(bossList).toEqual(expect.arrayContaining(['userId#4']));
  });
});

describe('Test getUserFeature', () => {

  test('getUserFeature is defined', () => {
    expect(getUserFeature).toBeDefined();
  });

  test('user is null', () => {
    const userFeature = getUserFeature(null);

    expect(typeof userFeature).toBe('undefined');
  });

  test('user is defined', () => {
    const userFeature = getUserFeature({
      userId: 'userId',
      userName: 'userName',
      fullName: 'fullName',

      roleList: [
        {
          roleId: 'roleId#1',
          roleCode: 'code#1',
          roleName: 'name#1',
        },
        {
          roleId: 'roleId#2',
          roleCode: 'code#2',
          roleName: 'name#2',
        },
        {
          roleId: 'roleId#3',
          roleCode: 'code#3',
          roleName: 'name#3',
        },
      ],

      departmentList: [
        {
          departmentId: 'departmentId#1',
          departmentCode: 'code#1',
          departmentName: 'name#1',

          companyId: 'companyId#1',
          companyCode: 'code#1',
          companyName: 'name#1',

          userList: [
            {
              titleId: TITLE.STAFF,
              titleCode: 'titleCode#1',
              titleName: 'titleName#1',
  
              userId: 'userId#1',
              userName: 'userName#1',
              fullName: 'fullName#1',
              email: 'fullName#1',
              phone: 'phone#1'
            },

            {
              titleId: TITLE.STAFF,
              titleCode: 'titleCode#2',
              titleName: 'titleName#2',
  
              userId: 'userId#2',
              userName: 'userName#2',
              fullName: 'fullName#2',
              email: 'fullName#2',
              phone: 'phone#2'
            }
          ],
        },

        {
          departmentId: 'departmentId#2',
          departmentCode: 'code#2',
          departmentName: 'name#2',

          companyId: 'companyId#2',
          companyCode: 'code#2',
          companyName: 'name#2',

          userList: [
            {
              titleId: TITLE.STAFF,
              titleCode: 'titleCode#1',
              titleName: 'titleName#1',
  
              userId: 'userId#3',
              userName: 'userName#3',
              fullName: 'fullName#3',
              email: 'fullName#3',
              phone: 'phone#3'
            },

            {
              titleId: TITLE.CTO,
              titleCode: 'titleCode#2',
              titleName: 'titleName#2',
  
              userId: 'userId#4',
              userName: 'userName#4',
              fullName: 'fullName#4',
              email: 'fullName#4',
              phone: 'phone#4'
            }
          ],
        },

        {
          departmentId: 'departmentId#3',
          departmentCode: 'code#3',
          departmentName: 'name#4',
          userList: [],
        },
      ]
    });

    expect(_.isObject(userFeature)).toBeTruthy();

    expect(userFeature.userId).toBe('userId');
    expect(userFeature.userName).toBe('userName');
    expect(userFeature.fullName).toBe('fullName');

    const { userFeatureList } = userFeature;

    expect(_.isArray(userFeatureList)).toBeTruthy();

    const companyIdList = userFeatureList.find(f => f.featureName === USER_FEATURE.COMPANY_ID_LIST);
    expect(companyIdList).toBeDefined();
    expect(_.isObject(companyIdList)).toBeTruthy();
    expect(companyIdList.value).toEqual(expect.arrayContaining(['companyId#1', 'companyId#2']));

    const departmentIdList = userFeatureList.find(f => f.featureName === USER_FEATURE.DEPARTMENT_ID_LIST);
    expect(departmentIdList).toBeDefined();
    expect(_.isObject(departmentIdList)).toBeTruthy();
    expect(departmentIdList.value).toEqual(expect.arrayContaining(['departmentId#1', 'departmentId#2', 'departmentId#3']));

    const bossUserId = userFeatureList.find(f => f.featureName === USER_FEATURE.BOSS_USER_ID);
    expect(bossUserId).toBeDefined();
    expect(_.isObject(bossUserId)).toBeTruthy();
    expect(bossUserId.value).toEqual(expect.arrayContaining(['userId#4']));

    const staffUserIdList = userFeatureList.find(f => f.featureName === USER_FEATURE.STAFF_ID_LIST);
    expect(staffUserIdList).toBeDefined();
    expect(_.isObject(staffUserIdList)).toBeTruthy();
    expect(staffUserIdList.value).toEqual(expect.arrayContaining(['userId#1', 'userId#2', 'userId#3']));

    const roleIdList = userFeatureList.find(f => f.featureName === USER_FEATURE.ROLE_ID_LIST);
    expect(roleIdList).toBeDefined();
    expect(_.isObject(roleIdList)).toBeTruthy();
    expect(roleIdList.value).toEqual(expect.arrayContaining(['roleId#1', 'roleId#2', 'roleId#3']));
  });
});

describe('Test getUserAccessList', () => {

  test('getUserAccessList is defined', () => {
    expect(getUserAccessList).toBeDefined();
  });

  test('userFeature parameter is null', () => {
    const accessList = getUserAccessList(null, []);

    expect(accessList).not.toBeDefined();
  });

  test('policyList parameter is null', () => {
    const accessList = getUserAccessList({
      userId: 'userId',
      userName: 'userName',
      fullName: 'fullName',
      userFeatureList: [],
    }, null);

    expect(accessList).not.toBeDefined();
  });

  test('ALL parameters is ok', () => {
    const generateUserFeature = (userId) => ({
      userId: `userId#${userId}`,
      userName: `userName#${userId}`,
      fullName: `fullName#${userId}`,

      userFeatureList: [
        {
          featureName: USER_FEATURE.ROLE_ID_LIST,
          value: [`roleId#${userId}` ],
        },
        {
          featureName: USER_FEATURE.COMPANY_ID_LIST,
          value: [`companyId#${userId}` ],
        },
      ],
    });

    const generateFunction = (functionId) => ({
      functionId: `5cc8a8a61d039008cbdd69a${functionId}`,
      functionUrl: `functionUrl#${functionId}`,
      functionName: `functionName#${functionId}`,
    });

    const generateService = (serviceId) => ({
      serviceId: `5cc8a8a61d039008cbdd69a${serviceId}`,
      serviceCode: `serviceCode#${serviceId}`,
      serviceName: `serviceName#${serviceId}`,

      actionId: `5cc8a8a61d039008cbdd69a${serviceId}`,
      actionCode: `actionCode#${serviceId}`,

      path: `path#${serviceId}`,
      method: `method#${serviceId}`,
    });

    const allowedRequestFieldList = ['requestField#1', 'requestField#2', 'requestField#3'];
    const allowedResponseFieldList = ['responseField#1', 'responseField#2'];
    // const allowedRequestFieldList2 = ['requestField#5', 'requestField#6', 'requestField#7'];
    // const allowedResponseFieldList2 = ['responseField#8', 'responseField#9'];

    const accessList = getUserAccessList(
      generateUserFeature('1'),
      {
        ...generateService('1'),
        ...generateFunction('1'),

        policyName: 'policyName#1',

        userFeatureList: [{
          featureName: USER_FEATURE.ROLE_ID_LIST,
          selectedOperator: OPERATOR.IN,
          selectedValueList: ['roleId#1', 'roleId#2'],
        }],

        allowedRequestFieldList,
        allowedResponseFieldList,

        recordFeatureList: [{
          featureName: 'featureName#1',
          type: 'string',
          selectedOperator: OPERATOR.EQ,
          isUserFeature: false,
          selectedValueList: [''],
        }],
      },
    );

    expect(accessList).toBeDefined();

    expect(_.isArray(accessList)).toBeTruthy();

    const p = accessList[0]; // cause of random ID issue => have to check manualy
    const f1 = generateFunction('1');

    expect({
      userId: p.userId,
      userName: p.userName,
      fullName: p.fullName,
      userFeatureList: p.userFeatureList,
    }).toEqual(generateUserFeature('1'));

    expect({
      functionUrl: p.functionUrl,
      functionName: p.functionName,
    }).toEqual({
      functionUrl: f1.functionUrl,
      functionName: f1.functionName,
    });

    expect(p.policyName).toEqual('policyName#1');

    expect(p.allowedRequestFieldList).toEqual(expect.arrayContaining(allowedRequestFieldList));
    expect(p.allowedResponseFieldList).toEqual(expect.arrayContaining(allowedResponseFieldList));

    expect(p.recordFeatureList).toEqual(expect.arrayContaining([{
      featureName: 'featureName#1',
      type: 'string',
      selectedOperator: OPERATOR.EQ,
      isUserFeature: false,
      selectedValueList: [''],
    }]));
  });
});

describe('Test checkFieldPermission', () => {

  test('checkFieldPermission is defined', () => {
    expect(checkFieldPermission).toBeDefined();
  });

  // requestFieldList, allowedFieldList
  test('requestFieldList and allowedFieldList is null', () => {
    const allowed = checkFieldPermission(null, null);

    expect(allowed).not.toBeTruthy();
  });

  test('requestFieldList is null', () => {
    const allowed = checkFieldPermission(null, ['field#1', 'field#2']);

    expect(allowed).not.toBeTruthy();
  });

  test('allowedFieldList is null', () => {
    const allowed = checkFieldPermission(['field#1', 'field#2'], null);

    expect(allowed).not.toBeTruthy();
  });

  test('same field list', () => {
    const allowed = checkFieldPermission(['field#1', 'field#2'], ['field#1', 'field#2']);

    expect(allowed).toBeTruthy();
  });

  test('allowed more than request', () => {
    const allowed = checkFieldPermission(['field#1', 'field#2'], ['field#1', 'field#2', 'field#3']);

    expect(allowed).toBeTruthy();
  });

  test('request more than allowed', () => {
    const allowed = checkFieldPermission(['field#1', 'field#2', 'field#3'], ['field#1', 'field#2']);

    expect(allowed).toBeTruthy();
  });
});

describe('Test checkRecordPermission', () => {
  test('checkRecordPermission is defined', () => {
    expect(checkRecordPermission).toBeDefined();
  });

  // requestFieldList, allowedFieldList
  test('requestFieldList, userFeatureList and allowedFieldList is null', () => {
    const allowed = checkRecordPermission(null, null, null);

    expect(allowed).not.toBeTruthy();
  });

  test('requestFieldList is null', () => {
    const allowed = checkRecordPermission(null, null, []);

    expect(allowed).not.toBeTruthy();
  });

  test('userFeatureList is null', () => {
    const allowed = checkRecordPermission({}, null, []);

    expect(allowed).not.toBeTruthy();
  });

  test('allowedFieldList is null', () => {
    const allowed = checkRecordPermission({}, [], null);

    expect(allowed).not.toBeTruthy();
  });

  test('check "createdBy" field', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdBy',
          type: DATA_TYPE.STRING,
          selectedOperator: OPERATOR.EQ,
          isUserFeature: true,
          selectedValueList: [USER_FEATURE.USER_ID],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "state" OK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        state: 'state#1',
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'state',
          type: DATA_TYPE.STRING,
          selectedOperator: OPERATOR.IN,
          isUserFeature: false,
          selectedValueList: ['state#1', 'state#2'],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "state" NOK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        state: 'state#1',
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'state',
          type: DATA_TYPE.STRING,
          selectedOperator: OPERATOR.IN,
          isUserFeature: false,
          selectedValueList: ['state#3', 'state#2'],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "interger" EQ OK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        total: 3,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'total',
          type: DATA_TYPE.NUMBER,
          selectedOperator: OPERATOR.EQ,
          isUserFeature: false,
          selectedValueList: ['3'],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "interger" EQ NOK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        total: 3,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'total',
          type: DATA_TYPE.NUMBER,
          selectedOperator: OPERATOR.EQ,
          isUserFeature: false,
          selectedValueList: ['5'],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "float" EQ OK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        total: 3.5,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'total',
          type: DATA_TYPE.NUMBER,
          selectedOperator: OPERATOR.EQ,
          isUserFeature: false,
          selectedValueList: ['3.5'],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "float" EQ NOK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        total: 3.5,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'total',
          type: DATA_TYPE.NUMBER,
          selectedOperator: OPERATOR.EQ,
          isUserFeature: false,
          selectedValueList: ['3.6'],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "float" LT OK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        total: 3.5,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'total',
          type: DATA_TYPE.NUMBER,
          selectedOperator: OPERATOR.LT,
          isUserFeature: false,
          selectedValueList: ['3.6'],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "float" LT NOK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        total: 3.5,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'total',
          type: DATA_TYPE.NUMBER,
          selectedOperator: OPERATOR.LT,
          isUserFeature: false,
          selectedValueList: ['3.3'],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "float" GT OK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        total: 3.5,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'total',
          type: DATA_TYPE.NUMBER,
          selectedOperator: OPERATOR.GT,
          isUserFeature: false,
          selectedValueList: ['3.3'],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "float" GT NOK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        total: 3.5,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'total',
          type: DATA_TYPE.NUMBER,
          selectedOperator: OPERATOR.GT,
          isUserFeature: false,
          selectedValueList: ['3.8'],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "float" GTE OK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        total: 3.5,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'total',
          type: DATA_TYPE.NUMBER,
          selectedOperator: OPERATOR.GTE,
          isUserFeature: false,
          selectedValueList: ['3.5'],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "float" GTE NOK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        total: 3.5,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'total',
          type: DATA_TYPE.NUMBER,
          selectedOperator: OPERATOR.GTE,
          isUserFeature: false,
          selectedValueList: ['3.8'],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "float" LTE OK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        total: 3.5,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'total',
          type: DATA_TYPE.NUMBER,
          selectedOperator: OPERATOR.LTE,
          isUserFeature: false,
          selectedValueList: ['3.5'],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "float" LTE NOK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        total: 3.5,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'total',
          type: DATA_TYPE.NUMBER,
          selectedOperator: OPERATOR.LTE,
          isUserFeature: false,
          selectedValueList: ['3.3'],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "array" IN OK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        productList: ['product#1'],
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'productList',
          type: DATA_TYPE.ARRAY,
          selectedOperator: OPERATOR.IN,
          isUserFeature: false,
          selectedValueList: ['product#1', 'product#2', 'product#3'],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "array" IN NOK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        productList: ['product#1'],
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'productList',
          type: DATA_TYPE.ARRAY,
          selectedOperator: OPERATOR.IN,
          isUserFeature: false,
          selectedValueList: ['product#2', 'product#3'],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "array" NOT_IN OK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        productList: ['product#1'],
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'productList',
          type: DATA_TYPE.ARRAY,
          selectedOperator: OPERATOR.NOT_IN,
          isUserFeature: false,
          selectedValueList: ['product#2', 'product#3'],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "array" NOT_IN NOK', () => {
    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        productList: ['product#1'],
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'productList',
          type: DATA_TYPE.ARRAY,
          selectedOperator: OPERATOR.NOT_IN,
          isUserFeature: false,
          selectedValueList: ['product#1', 'product#2', 'product#3'],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "date" EQ OK', () => {
    const checkedDate = new Date();

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: checkedDate,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE,
          selectedOperator: OPERATOR.EQ,
          isUserFeature: false,
          selectedValueList: [checkedDate.toString()],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "date" EQ NOK', () => {
    const date1 = new Date();
    const date2 = date1.setFullYear(2018);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE,
          selectedOperator: OPERATOR.EQ,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "date" NEQ OK', () => {
    const date1 = new Date();
    const date2 = date1.setFullYear(2018);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE,
          selectedOperator: OPERATOR.NEQ,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "date" NEQ NOK', () => {
    const checkedDate = new Date();

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: checkedDate,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE,
          selectedOperator: OPERATOR.NEQ,
          isUserFeature: false,
          selectedValueList: [checkedDate.toString()],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "date" GT OK', () => {
    const date1 = new Date();
    const date2 = new Date(2000, 1, 1);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE,
          selectedOperator: OPERATOR.GT,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "date" GT NOK', () => {
    const date1 = new Date();
    const date2 = new Date(2099, 1, 1);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE,
          selectedOperator: OPERATOR.GT,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "date" GTE OK', () => {
    const date1 = new Date();

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE,
          selectedOperator: OPERATOR.GTE,
          isUserFeature: false,
          selectedValueList: [date1.toString()],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "date" GTE NOK', () => {
    const date1 = new Date();
    const date2 = new Date(2099, 1, 1);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE,
          selectedOperator: OPERATOR.GTE,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "date" LT OK', () => {
    const date1 = new Date();
    const date2 = new Date(2099, 1, 1);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE,
          selectedOperator: OPERATOR.LT,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "date" LT NOK', () => {
    const date1 = new Date();

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE,
          selectedOperator: OPERATOR.LT,
          isUserFeature: false,
          selectedValueList: [date1.toString()],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "date" LTE OK', () => {
    const date1 = new Date();

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE,
          selectedOperator: OPERATOR.LTE,
          isUserFeature: false,
          selectedValueList: [date1.toString()],
        }
      ]);

    expect(allowed).toBeTruthy();
  });


  test('check "date" LTE NOK', () => {
    const date1 = new Date();
    const date2 = new Date(2000, 1, 1);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE,
          selectedOperator: OPERATOR.LTE,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "datetime" EQ OK', () => {
    const checkedDate = new Date();

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: checkedDate,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE_TIME,
          selectedOperator: OPERATOR.EQ,
          isUserFeature: false,
          selectedValueList: [checkedDate.toString()],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "datetime" EQ NOK', () => {
    const date1 = new Date();
    const date2 = date1.setFullYear(2018);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE_TIME,
          selectedOperator: OPERATOR.EQ,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "datetime" NEQ OK', () => {
    const date1 = new Date();
    const date2 = date1.setFullYear(2018);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE_TIME,
          selectedOperator: OPERATOR.NEQ,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "datetime" NEQ NOK', () => {
    const checkedDate = new Date();

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: checkedDate,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE_TIME,
          selectedOperator: OPERATOR.NEQ,
          isUserFeature: false,
          selectedValueList: [checkedDate.toString()],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "datetime" GT OK', () => {
    const date1 = new Date();
    const date2 = new Date(2000, 1, 1);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE_TIME,
          selectedOperator: OPERATOR.GT,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "datetime" GT NOK', () => {
    const date1 = new Date();
    const date2 = new Date(2099, 1, 1);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE_TIME,
          selectedOperator: OPERATOR.GT,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "datetime" GTE OK', () => {
    const date1 = new Date();

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE_TIME,
          selectedOperator: OPERATOR.GTE,
          isUserFeature: false,
          selectedValueList: [date1.toString()],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "datetime" GTE NOK', () => {
    const date1 = new Date();
    const date2 = new Date(2099, 1, 1);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE_TIME,
          selectedOperator: OPERATOR.GTE,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "datetime" LT OK', () => {
    const date1 = new Date();
    const date2 = new Date(2099, 1, 1);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE_TIME,
          selectedOperator: OPERATOR.LT,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).toBeTruthy();
  });

  test('check "datetime" LT NOK', () => {
    const date1 = new Date();

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE_TIME,
          selectedOperator: OPERATOR.LT,
          isUserFeature: false,
          selectedValueList: [date1.toString()],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

  test('check "datetime" LTE OK', () => {
    const date1 = new Date();

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE_TIME,
          selectedOperator: OPERATOR.LTE,
          isUserFeature: false,
          selectedValueList: [date1.toString()],
        }
      ]);

    expect(allowed).toBeTruthy();
  });


  test('check "datetime" LTE NOK', () => {
    const date1 = new Date();
    const date2 = new Date(2000, 1, 1);

    const allowed = checkRecordPermission(
      {
        _id: 'id#1',
        createdBy: 'userId#1',
        createdDate: date1,
      },

      [
        {
          featureName: USER_FEATURE.USER_ID,
          value: ['userId#1'],
        }
      ],

      [
        {
          featureName: 'createdDate',
          type: DATA_TYPE.DATE_TIME,
          selectedOperator: OPERATOR.LTE,
          isUserFeature: false,
          selectedValueList: [date2.toString()],
        }
      ]);

    expect(allowed).not.toBeTruthy();
  });

});