import humps from 'humps';
import pluralize from 'pluralize';
import mongoose from 'mongoose';
import _ from 'lodash';
import debug from 'debug';
import colors from 'colors';
import handlebars from 'handlebars';
import warning from 'tiny-warning';

import sb from './serviceBusHelper';
import { getEvent } from './eventHelper';
import { commonEvent } from '../constants/eventConstant';
import { ITEM_AMOUNT_PER_PAGE, DELETE_REJECT_TIME_OUT_DURATION } from '../constants/commonConstant';
import { generateSwagger } from './swaggerHelper';
import { HTTP_RESPONSE_CODE } from '../constants/httpConstant';

import {
  generateGetListActionCode,
  generateGetByIdActionCode,
  generateCreateActionCode,
  generateUpdateActionCode,
  generateDeleteActionCode,
} from './swaggerHelper';

import {
  checkFieldPermission,
  checkRecordPermission,
} from './policyHelper';
import { DATA_TYPE, OPERATOR } from '../constants/dataTypeConstant';

const serviceBusDebugger = debug('app:sb');
const appBizDebugger = debug('app:biz');

const AccessList = mongoose.model('sysAccessList');
const UserFeatureModel = mongoose.model('sysUserFeatures');
export const FULL_FIELD_LIST = '*';
export const getModelName = baseUrl => baseUrl.replace(/^\/v\d\//i, '');
export const QUERY_RESERVED_FIELDS = ['limit', 'offset', 'sortBy', 'fields'];

export const MODEL_RESERVED_FIELDS = [
  '_id',
  'createdBy', 'createdByUserName', 'createdByFullName', 'createdAt',
  'updatedBy', 'updatedByUserName', 'updatedByFullName', 'updatedAt',
];

export const pascalizeAndSingular = (modelName) => {
  return pluralize.singular(humps.pascalize(modelName))
}

const getQueryPermission = async (actionCode, user, responseFieldList) => {
  const { userId, isAdmin, functionId } = user;

  if (!userId || !functionId) {
    warning(!userId, 'userId is undefined.');
    warning(!functionId, 'functionId is undefined.');
    return [];
  }

  if (isAdmin) { // full permission
    warning(true, 'user is admin.');

    const userFeature = await UserFeatureModel.findOne({ userId }, { userFeatureList: 1 });

    return ([{
      allowedRequestFieldList: [],
      allowedResponseFieldList: [],
      userFeatureList: userFeature ? userFeature.userFeatureList : [],
      recordFeatureList: [],
    }]);
  }
  const accessList = await AccessList.find({ userId, functionId, actionCode });
  const accessListCount = accessList ? accessList.length : undefined;
  const finalAccessList = [];

  if (!accessListCount) {
    warning(true, 'accessList is undefined.');
    return [];
  }

  for (let i = 0; i < accessListCount; i += 1) {
    const { allowedResponseFieldList } = accessList[i];
    const fieldListIsOk = checkFieldPermission(responseFieldList, allowedResponseFieldList);

    if (fieldListIsOk) {
      finalAccessList.push(accessList[i]);
    }
  }

  return finalAccessList;
}

// can READ record by Id
const getReadPermission = async (actionCode, user, responseFieldList, dataObject) => {
  const { userId, isAdmin, functionId } = user;

  if (!userId || !functionId) {
    return null;
  }

  if (isAdmin) { // full permission
    const userFeature = await UserFeatureModel.findOne({ userId }, { userFeatureList: 1 });

    return ({
      allowedRequestFieldList: [FULL_FIELD_LIST],
      allowedResponseFieldList: [FULL_FIELD_LIST],
      userFeatureList: userFeature ? userFeature.userFeatureList : [],
      recordFeatureList: [],
    });
  }

  const accessList = await AccessList.find({ userId, functionId, actionCode });
  const accessListCount = accessList ? accessList.length : undefined;

  if (!accessListCount) {
    return null;
  }

  for (let i = 0; i < accessListCount; i += 1) {
    const { allowedResponseFieldList, userFeatureList, recordFeatureList } = accessList[i];
    const fieldListIsOk = checkFieldPermission(responseFieldList, allowedResponseFieldList);
    const recordIsOk = checkRecordPermission(dataObject, userFeatureList, recordFeatureList);

    if (fieldListIsOk && recordIsOk) {
      return accessList[i];
    }
  }

  return null;
}

// can CREATE / UPDATE / DELETE / ... record by Id
export const getCRUDPermission = async (actionCode, user, requestFieldList, dataObject) => {
  const { userId, isAdmin, functionId } = user;

  if (!userId || !functionId) {
    return null;
  }

  if (isAdmin) { // full permission
    const userFeature = await UserFeatureModel.findOne({ userId }, { userFeatureList: 1 });

    return ({
      allowedRequestFieldList: [FULL_FIELD_LIST],
      allowedResponseFieldList: [FULL_FIELD_LIST],
      userFeatureList: userFeature ? userFeature.userFeatureList : [],
      recordFeatureList: [],
    });
  }

  const accessList = await AccessList.find({ userId, functionId, actionCode });
  const accessListCount = accessList ? accessList.length : undefined;

  if (!accessListCount) {
    return null;
  }

  for (let i = 0; i < accessListCount; i += 1) {
    const { allowedRequestFieldList, userFeatureList, recordFeatureList } = accessList[i];
    const fieldListIsOk = checkFieldPermission(requestFieldList, allowedRequestFieldList);
    const recordIsOk = checkRecordPermission(dataObject, userFeatureList, recordFeatureList);

    if (fieldListIsOk && recordIsOk) {
      return accessList[i];
    }
  }

  return null;
}

export const healthCheckController = (req, res, next) => {
  res.status(HTTP_RESPONSE_CODE.OK).json({
    status: "Hi! I'm good!",
  });
}

export const swaggerController = (req, res) => {
  const { baseUrl } = req;
  const model = getModelName(baseUrl);
  const version = baseUrl.charAt(2); // TODO: bug if version greater than 9 (2 digits)

  const swagger = generateSwagger(model, version);

  if (swagger) {
    res.status(HTTP_RESPONSE_CODE.OK).json(swagger);
  } else {
    res.status(HTTP_RESPONSE_CODE.NOT_FOUND);
  }
}

export const idParamController = (req, res, next, id) => {
  // if (req.method === "GET") { // just findById if HTTP METHOD is UPDATE / DELETE
  //   return next();
  // }

  const modelName = getModelName(req.baseUrl);
  const RequestedDataModel = mongoose.model(modelName);
  const dataSchemaTree = RequestedDataModel.schema.obj;

  RequestedDataModel.findOne({
    _id: id,
    deleted: {
      $ne: true,
    }
  })
    .then((dataObject) => {
      if (!dataObject) { return res.sendStatus(HTTP_RESPONSE_CODE.NOT_FOUND); }

      req.dataObject = dataObject;
      req.dataSchemaTree = dataSchemaTree;

      return next();
    }).catch(next);
};

export const queryHandler = async (req, res, next, cb) => {
  const { baseUrl, user, query } = req;
  const modelName = getModelName(baseUrl);
  const actionCode = generateGetListActionCode(pascalizeAndSingular(modelName));
  const { fields, sortBy, exact } = query;
  const responseFieldList = fields ? fields.split(',') : [];
  const exactList = exact ? exact.split(',') : [];
  const permissionList = await getQueryPermission(actionCode, user, responseFieldList);

  appBizDebugger(`actionCode: ${actionCode}`);
  appBizDebugger(`user: ${JSON.stringify(user)}`);
  appBizDebugger(`responseFieldList: ${JSON.stringify(responseFieldList)}`);
  appBizDebugger(`=> found permissionList.length = ${_.isArray(permissionList) ? permissionList.length : 0}`);

  if (_.isArray(permissionList)) {
    permissionList.forEach((perm, index) => {
      appBizDebugger(`permission ${index + 1}: ${JSON.stringify(perm)}\n`);
    });
  }

  if (permissionList.length === 0) {
    res.status(HTTP_RESPONSE_CODE.FORBIDDEN).json({ error: 'Forbidden: Can not get permission list' });
    return;
  }

  const RequestedDataModel = mongoose.model(modelName);
  const limit = typeof query.limit !== 'undefined' ? Number(query.limit) : ITEM_AMOUNT_PER_PAGE;
  const offset = typeof query.offset !== 'undefined' ? Number(query.offset) : 0;
  const dataSchemaTree = RequestedDataModel.schema.obj;

  const dataQuery = {};
  Object.entries(query).forEach(([key, value]) => {
    if (QUERY_RESERVED_FIELDS.indexOf(key) < 0) {
      const splittedFieldKey = key.split('.');
      const rootFieldKey = splittedFieldKey[0];
      const extFieldKey = splittedFieldKey.length > 1 ? splittedFieldKey[1] : '';
      const field = dataSchemaTree[rootFieldKey]; // check type of try to search sub-document
 
      if (typeof field !== 'undefined') {
        if (_.isArray(value)) {
          dataQuery[key] = { $in: value };
        } if (_.isObject(value)) {
          if (value.$exists) {
            dataQuery[key] = {};
            dataQuery[key].$exists = value.$exists === 'true';
          } else {
            dataQuery[key] = value;
          }
        } else {
          const dataType = (typeof field.type !== 'undefined') ? field.type.name : field.name; 

          switch (dataType) {
            case 'String': // mongoose string type
              if ((typeof value) === 'string' && !exactList.includes(key)) {
                dataQuery[key] = { $regex: value, $options: 'i' };
              } else {
                dataQuery[key] = value;
              }
              break;
            case 'ObjectId':
              if (value && (value !== '0') && (value !== '')) { // skip 0, '', null value
                dataQuery[key] = value;
              }
              break;
            case 'Date': {
              let dateLookup;

              // TODO: check lai phan thoi gian tham khao controllers/serviceHistoryController.js

              switch (extFieldKey) {
                case '$lt':
                  dateLookup = { $lt: value };
                  break;
                case '$lte':
                  dateLookup = { $lte: value };
                  break;
                case '$gt':
                  dateLookup = { $gt: value };
                  break;
                case '$gte':
                  dateLookup = { $gte: value };
                  break;
                default:
                  dateLookup = { $eq: value };
                  break;
              }

              if (typeof dataQuery[rootFieldKey] === 'undefined') {
                dataQuery[rootFieldKey] = dateLookup;
              } else {
                dataQuery[rootFieldKey] = _.extend(dataQuery[rootFieldKey], dateLookup);
              }
              break;
            }
            default:
              dataQuery[key] = value;
              break;
          }
        }
      } else if (key === '_id') {
        dataQuery._id = value;
      } else if (key === '$or' || key === '$and') { // mongo query
        dataQuery[key] = value;
      }
    }
  });

  dataQuery.deleted = { // reject deleted record
    $ne: true,
  };

  const fieldSet = {};
  responseFieldList.forEach((field) => {
    fieldSet[field.trim()] = 1;
  });

  const sortBySet = {};
  if (sortBy) {
    sortBy.split(',').forEach((condition) => {
      const field = condition.trim();
      if (field.length > 0) {
        const splitCondition = field.split('.'); // template: fieldName.desc / fieldName.asc
        const orderBy = splitCondition[0];

        if (typeof dataSchemaTree[orderBy] !== 'undefined') {
          let orderDirection;

          if (splitCondition.length === 2) {
            if (splitCondition[1] === 'desc') {
              orderDirection = -1;
            } else {
              orderDirection = 1;
            }
          } else {
            orderDirection = 1;
          }
          sortBySet[orderBy] = orderDirection;
        }
      }
    });
  } else { // auto sort by ...
    const modelFieldName = humps.camelize(`${pluralize.singular(modelName)}Name`); // name field of data model

    if (typeof dataSchemaTree[modelFieldName] !== 'undefined') {
      sortBySet[modelFieldName] = 1;
    } else {
      sortBySet._id = -1;
    }
  }

  const finalQuery = [];
  permissionList.forEach((perm, index) => {
    const { userFeatureList, recordFeatureList } = perm;
    const tmpQuery = {};

    appBizDebugger(`permissionList index: ${index}`);
    appBizDebugger(`userFeatureList: ${userFeatureList}`);
    appBizDebugger(`recordFeatureList: ${recordFeatureList}`);

    if (_.isArray(recordFeatureList)) {
      for (let i = 0; i < recordFeatureList.length; i += 1) {
        let operatedByValue;

        const {
          featureName, selectedOperator, type,
          isUserFeature, selectedValueList,
        } = recordFeatureList[i];
        appBizDebugger(`recordFeatureList: ${JSON.stringify(recordFeatureList)}`);

        if (isUserFeature) {
          if (!selectedValueList || !selectedValueList.length) {
            appBizDebugger('!selectedValueList');
            continue;
          }
          appBizDebugger(`userFeatureList: ${JSON.stringify(userFeatureList)}`);
          appBizDebugger(`selectedValueList: ${selectedValueList}`);

          const userFeature = userFeatureList.find(f => f.featureName === selectedValueList);

          if (_.isUndefined(userFeature)) {
            appBizDebugger('!userFeature');
            continue;
          } else {
            operatedByValue = userFeature.value;
            appBizDebugger(`operatedByValue: ${operatedByValue}`);
          }
        } else { // if (isUserFeature)
          operatedByValue = selectedValueList;
        } // if (isUserFeature)

        if (_.isUndefined(operatedByValue)) {
          appBizDebugger('!operatedByValue');
          continue;
        }

        switch (selectedOperator) {
          case OPERATOR.EQ:
          case OPERATOR.NEQ: {
            switch (type) {
              case DATA_TYPE.ARRAY:
              case DATA_TYPE.EMAIL:
              case DATA_TYPE.PHONE:
              case DATA_TYPE.OBJECT:
                break; // do nothing

              case DATA_TYPE.DATE:
              case DATA_TYPE.DATE_TIME: {
                tmpQuery[featureName] = {
                  [selectedOperator]: new Date(operatedByValue),
                };

                break;
              }

              case DATA_TYPE.ID: {
                tmpQuery[featureName] = {
                  [selectedOperator]: mongoose.Types.ObjectId(operatedByValue),
                };

                break;
              }

              default: { // number / boolean / string
                tmpQuery[featureName] = {
                  [selectedOperator]: operatedByValue,
                };

                break;
              }
            } // switch (type)

            break;
          }

          case OPERATOR.GT:
          case OPERATOR.GTE:
          case OPERATOR.LT:
          case OPERATOR.LTE: {
            switch (type) {
              case DATA_TYPE.ARRAY:
              case DATA_TYPE.EMAIL:
              case DATA_TYPE.PHONE:
              case DATA_TYPE.OBJECT:
              case DATA_TYPE.ID:
                break; // do nothing

              case DATA_TYPE.DATE:
              case DATA_TYPE.DATE_TIME: {
                tmpQuery[featureName] = {
                  [selectedOperator]: new Date(operatedByValue),
                };

                break;
              }

              default: { // NUMBER / string
                tmpQuery[featureName] = {
                  [selectedOperator]: operatedByValue,
                };

                break;
              }
            } // switch (type)

            break;
          }

          case OPERATOR.IN:
          case OPERATOR.NOT_IN: {
            switch (type) {
              case DATA_TYPE.ID:
              case DATA_TYPE.NUMBER:
              case DATA_TYPE.STRING:
              case DATA_TYPE.BOOLEAN: {
                tmpQuery[featureName] = {
                  [selectedOperator]: _.isArray(operatedByValue) ? operatedByValue : operatedByValue.split(','),
                };

                break;
              }

              case DATA_TYPE.DATE: // not support yet
              case DATA_TYPE.DATE_TIME:
                break;

              default: {
                break;
              }
            } // switch (type)

            break;
          }
        } // switch (selectedOperator)

        appBizDebugger(`tmpQuery: ${JSON.stringify(tmpQuery)}`);
      }
    } // if (_.isArray(recordFeatureList))

    finalQuery.push(_.merge(tmpQuery, dataQuery));
  });

  const uniqueFinalQuery = _.uniqWith(finalQuery, _.isEqual); // TODO: get unique ís not working
  const uniqueFinalQueryCount = uniqueFinalQuery.length;

  appBizDebugger(`uniqueFinalQuery: ${JSON.stringify(uniqueFinalQuery)}`);

  if (uniqueFinalQueryCount === 1) {
    const callbackAlias1 = (results) => { cb(modelName, uniqueFinalQuery[0], offset, limit, results); }

    return Promise.all([
      RequestedDataModel.find(uniqueFinalQuery[0])
        .select(fieldSet)
        .limit(limit)
        .skip(offset)
        .sort(sortBySet)
        .exec(),
      RequestedDataModel.count(uniqueFinalQuery[0]).exec(),
    ]).then(callbackAlias1).catch(next);
  } else if (uniqueFinalQueryCount > 1) {
    const callbackAlias2 = (results) => { cb(modelName, uniqueFinalQuery, offset, limit, results); }

    return Promise.all([
      RequestedDataModel.find({ $or: uniqueFinalQuery })
        .select(fieldSet)
        .limit(limit)
        .skip(offset)
        .sort(sortBySet)
        .exec(),
      RequestedDataModel.count({ $or: uniqueFinalQuery }).exec(),
    ]).then(callbackAlias2).catch(next);
  }

  const callbackAlias3 = (results) => { cb(modelName, uniqueFinalQuery, offset, limit, results); }

  return Promise.all([
    RequestedDataModel.find({})
      .select(fieldSet)
      .limit(limit)
      .skip(offset)
      .sort(sortBySet)
      .exec(),
    RequestedDataModel.count({}).exec(),
  ]).then(callbackAlias3).catch(next);
};

export const queryController = async (req, res, next) => {
  await queryHandler(req, res, next,
    (modelName, dataQuery, offset, limit, results) => res.json({
      model: modelName,
      data: results[0],
      length: results[1],
      query: { ...dataQuery, offset, limit },
    }));
};

export const readController = async (req, res, next) => {
  const { baseUrl, user, dataObject, query } = req;
  const modelName = getModelName(baseUrl);
  const actionCode = generateGetByIdActionCode(pascalizeAndSingular(modelName));
  const responseFieldList = query.fields ? query.fields.split(',') : [];
  const perm = await getReadPermission(actionCode, user, responseFieldList, dataObject);

  if (!perm) {
    res.status(HTTP_RESPONSE_CODE.FORBIDDEN).json({ error: 'Forbidden: Can not get permission list' });
    return;
  }

  return res.json({ data: _.pick(dataObject, responseFieldList) });
};

export const createController = async (req, res, next) => {
  const { baseUrl, user, body } = req;
  const modelName = getModelName(baseUrl);
  const actionCode = generateCreateActionCode(pascalizeAndSingular(modelName));
  const RequestedDataModel = mongoose.model(modelName);

  const dataObject = new RequestedDataModel();
  const requestFieldList = [];

  Object.entries(body).forEach(([key, value]) => {
    if (MODEL_RESERVED_FIELDS.indexOf(key) < 0) {
      dataObject[key] = value;
      requestFieldList.push(key);
    }
  });

  const perm = await getCRUDPermission(actionCode, user, requestFieldList, dataObject);

  if (!perm) {
    res.status(HTTP_RESPONSE_CODE.FORBIDDEN).json({ error: 'Forbidden' });
    return;
  }

  dataObject.createdBy = user.userId;
  dataObject.createdByUserName = user.userName;
  dataObject.createdByFullName = user.fullName;

  return dataObject.save().then((data) => {
    const eventType = getEvent(modelName, commonEvent.CREATED);

    sb.publish(modelName, eventType, { model: modelName, oldData: {}, newData: data }, '0', (err) => {
      if (err) {
        serviceBusDebugger(`[SB] Publish error: ${colors.red(err)}`);
      }
    });

    return res.status(HTTP_RESPONSE_CODE.OK).json({ data });
  }).catch(next);
};

export const updateController = async (req, res, next) => {
  try {
    const { baseUrl, user, dataObject, body } = req;
    const modelName = getModelName(baseUrl);
    const actionCode = generateUpdateActionCode(pascalizeAndSingular(modelName));
    const RequestedDataModel = mongoose.model(modelName);
    const dataSchemaTree = RequestedDataModel.schema.obj;
    const requestedDataModelFields = _.difference(_.keys(dataSchemaTree), MODEL_RESERVED_FIELDS);
    const oldData = _.clone(dataObject._doc);
    let objectChanged = false;
    const requestFieldList = [];

    requestedDataModelFields.forEach((fieldName) => {
      const value = body[fieldName];

      if (!_.isUndefined(value)) {
        if (!_.isEqual(dataObject[fieldName], value)) { // if changed
          requestFieldList.push(fieldName);

          dataObject[fieldName] = value;
          objectChanged = true;
        }
      }
    });

    const perm = await getCRUDPermission(actionCode, user, requestFieldList, dataObject);

    if (!perm) {
      res.status(HTTP_RESPONSE_CODE.FORBIDDEN).json({ error: 'Forbidden' });
      return;
    }

    dataObject.updatedBy = user.userId;
    dataObject.updatedByUserName = user.userName;
    dataObject.updatedByFullName = user.fullName;

    dataObject.save().then((newData) => {
      if (objectChanged) {
        const eventType = getEvent(modelName, commonEvent.UPDATED);

        sb.publish(modelName, eventType, { model: modelName, oldData, newData }, '0', (err) => {
          if (err) {
            serviceBusDebugger(`[SB] Publish error: ${colors.red(err)}`);
          }
        });
      }

      // return res.json({ data: newData }); // TODO: LIMIT response data field list
      return res.sendStatus(HTTP_RESPONSE_CODE.OK);
    }).catch(next);
  }
  
  catch (error) {
    next(error);
  }


};

export const deleteController = async (req, res) => {
  const { baseUrl, user, dataObject, dataSchemaTree } = req;
  const modelName = getModelName(baseUrl);
  const actionCode = generateDeleteActionCode(pascalizeAndSingular(modelName));
  const perm = await getCRUDPermission(actionCode, user, [], dataObject);

  if (!perm) {
    res.status(HTTP_RESPONSE_CODE.FORBIDDEN).json({ error: 'Forbidden' });
    return;
  }

  const now = new Date();
  const timeout = now;
  timeout.setMinutes(timeout.getMinutes() + DELETE_REJECT_TIME_OUT_DURATION);

  const fieldUniqueList = [];
  Object.entries(dataSchemaTree).forEach(([key, value]) => {
    if (value.unique){
      fieldUniqueList.push(key)
    }
  });

  fieldUniqueList.forEach(element => {
    dataObject[element] = `${dataObject[element]}[deleted.${Date.now()}]`;
  });

  dataObject.deleteRejectTimeOut = timeout;
  dataObject.deleted = true;
  dataObject.deletedAt = now;
  dataObject.deletedBy = user.userId;
  dataObject.deletedByUserName = user.userName;
  dataObject.deletedByFullName = user.fullName;

  return dataObject.save().then((data) => {
    const eventType = getEvent(modelName, commonEvent.DELETED);

    sb.publish(modelName, eventType, data._id, '0', (err) => { // TODO: add rejected service info
      if (err) {
        serviceBusDebugger(`[SB] Publish error: ${colors.red(err)}`);
      }
    });

    return res.sendStatus(HTTP_RESPONSE_CODE.OK);
  });
};

export const processApprove = async (dataObject, processConstant, accepted, actionCode, functionId, state) => {
  const oldState = dataObject[state];
  const stateObject = processConstant.find(s => s.current == oldState);

  const branches = stateObject.branches !== undefined ? stateObject.branches : [];

  let flow = {};

  flow.previous = stateObject.previous
  flow.next = stateObject.next

  // branches.forEach(branch => {
  for (let branch of branches) {
    let k = 0;
    branch.conditions.forEach(condition => {
      switch (condition.operator) {
        case 'eq':
          if (dataObject[condition.field] === condition.value) k++;
          break;
      
        case 'ne':
          if (dataObject[condition.field] !== condition.value) k++;
          break;
      
        case 'lt':
          if (dataObject[condition.field] < condition.value) k++;
          break;
      
        case 'lte':
          if (dataObject[condition.field] <= condition.value) k++;
          break;
      
        case 'gt':
          if (dataObject[condition.field] > condition.value) k++;
          break;
      
        case 'gte':
          if (dataObject[condition.field] >= condition.value) k++;
          break;
      
        case 'in':
          if (_.difference(dataObject[condition.field], condition.value).length >= 0) k++
          break;
      
        case 'nin':
          if (_.difference(dataObject[condition.field], condition.value).length == 0) k++
          break;
      
        default:
          break;
      }
      
    });

    // let flow = {};
    switch (branch.operator) {
      case 'or':
        if (k > 0) {
          flow.previous = branch.previous;
          flow.next = branch.next;
        } else {
          flow.previous = stateObject.previous
          flow.next = stateObject.next
        }
        break;
    
      case 'and':
        if (k == branch.conditions.length) {
          flow.previous = branch.previous;
          flow.next = branch.next;
        } else {
          flow.previous = stateObject.previous
          flow.next = stateObject.next
        }
        break;
    
      default:
        flow.previous = stateObject.previous
        flow.next = stateObject.next
        break;
    }

    if (k > 0) break;

  }

  if (accepted) {
    let flowMail = flow.next;
    let newState = flowMail.state;
    const index = processConstant.findIndex(s => s.current == newState);
    dataObject[state] = newState;
    let toMailList = [];
    let ccMailList = [`${dataObject.createdByUserName}@fpt.com.vn`];
    if (index == -1) {
      toMailList = [`${dataObject.createdByUserName}@fpt.com.vn`];
    } else {
      toMailList = await getToMailList(dataObject, actionCode, functionId, newState);
    }
    // send mail
    if (flowMail.mail.ccMail != undefined) {
      for (let stateCC of flowMail.mail.ccMail) {
        let ccUserList = await AccessList.find({actionCode, functionId, 'recordFeatureList.selectedValueList': stateCC }, {userName: 1});
        ccUserList.forEach(cc => {
          ccMailList.push(`${cc.userName}@fpt.com.vn`);
        });
      }
    }

    return ({
      mail: sendMail(dataObject, flowMail.mail, toMailList, ccMailList),
      newState
    })
  } else {
    let flowMail = flow.previous;
    let newState = flowMail.state;
    const index = processConstant.findIndex(s => s.current == newState);
    dataObject[state] = newState;
    let toMailList = [];
    let ccMailList = [`${dataObject.createdByUserName}@fpt.com.vn`];
    if (index == 0) {
      toMailList = [`${dataObject.createdByUserName}@fpt.com.vn`];
    } else {
      toMailList = await getToMailList(dataObject, actionCode, functionId, newState);
    }
    // send mail
    if (flowMail.mail.ccMail != undefined) {
      for (let stateCC of flowMail.mail.ccMail) {
        let ccUserList = await AccessList.find({actionCode, functionId, 'recordFeatureList.selectedValueList': stateCC }, {userName: 1});
        ccUserList.forEach(cc => {
          ccMailList.push(`${cc.userName}@fpt.com.vn`);
        });
      }
    }
    return ({
      mail: sendMail(dataObject, flowMail.mail, toMailList, ccMailList),
      newState
    })
  }
}

async function getToMailList(dataObject, actionCode, functionId, newState) {
  const toMailList = [];
  const userList = await AccessList.find({ actionCode, functionId, 'recordFeatureList.selectedValueList': newState }, { userId: 1, userName: 1, recordFeatureList: 1, userFeatureList: 1 });
  const illegalUserList = [];
  for (let i = 0; i < userList.length; i++) {
    const user = userList[i];
    const { userFeatureList } = user;
    if (userFeatureList) {
      const { recordFeatureList } = user;
      const recordFeatureCount = recordFeatureList.length;
      let isLegal = false;
      for (let j = 0; j < recordFeatureCount; j += 1) {
        const recordFeature = recordFeatureList[j];
        const userFeatureIndex = userFeatureList.findIndex(u => u.featureName == recordFeature.selectedValueList && recordFeature.isUserFeature == true);
        console.log('hungnv104 -> userFeatureIndex:', userFeatureIndex);
        if (userFeatureIndex > -1) {
          for (let k = 0; k < userFeatureList[userFeatureIndex].value.length; k += 1) {
            const uf = userFeatureList[userFeatureIndex].value[k];
            console.log('hungnv104 -> uf:', uf);
            switch (recordFeature.selectedOperator) {
              
              case '$eq':
                if (dataObject[recordFeature.selectedValueList] == uf) {
                  isLegal = true;
                }
                break;
            
              case '$neq':
                if (dataObject[recordFeature.selectedValueList] !== uf) {
                  isLegal = true;
                }
                break;
            
              case '$lt':
                if (dataObject[recordFeature.selectedValueList] < uf) {
                  isLegal = true;
                }
                break;
            
              case '$lte':
                if (dataObject[recordFeature.selectedValueList] <= uf) {
                  isLegal = true;
                }
                break;
            
              case '$gt':
                if (dataObject[recordFeature.selectedValueList] > uf) {
                  isLegal = true;
                }
                break;
            
              case '$gte':
                if (dataObject[recordFeature.selectedValueList] >= uf) {
                  isLegal = true;
                }
                break;
            
              case '$in':
                for (let h = 0; h < dataObject[recordFeature.selectedValueList].length; h += 1) {
                  const feature = dataObject[recordFeature.selectedValueList][h];
                  const accessFeatureName = recordFeature.featureName.split('.')[recordFeature.featureName.split(".").length - 1];
                  
                  if (feature[accessFeatureName] == uf) {
                    isLegal = true;
                    break;
                  }
                }
                break;
            
              case '$nin':
                for (let h = 0; h < dataObject[recordFeature.selectedValueList].length; h += 1) {
                  const feature = dataObject[recordFeature.selectedValueList][h];
                  const accessFeatureName = recordFeature.featureName.split('.')[recordFeature.featureName.split(".").length - 1];
                  
                  if (feature[accessFeatureName] != uf) {
                    isLegal = true;
                    break;
                  }
                }
                break;
            
              case '$exists':
                for (let h = 0; h < dataObject[recordFeature.selectedValueList].length; h += 1) {
                  const feature = dataObject[recordFeature.selectedValueList][h];
                  const accessFeatureName = recordFeature.featureName.split('.')[recordFeature.featureName.split(".").length - 1];
                  if (feature[accessFeatureName] == uf) {
                    isLegal = true;
                    break;
                  }
                }
                break;

              default:
                break;
            }
            if (isLegal) break;
          }
          if (isLegal) break;
        } 
      }

      if (!isLegal) {
        illegalUserList.push(user);
      }
    }
  }
  
  // console.log('hungnv104 -> illegalUserList:', illegalUserList);
  // console.log('hungnv104 -> userList:', userList);
  const legalUserList = _.difference(userList, illegalUserList);
  // console.log('hungnv104 -> legalUserList:', legalUserList);
  // for (let i = 0; i < legalUserList.length; i += 1) {
  //   toMailList.push(`${legalUserList[i].userName}@fpt.com.vn`);
  // }
  for (let i = 0; i < userList.length; i += 1) {
    toMailList.push(`${userList[i].userName}@fpt.com.vn`);
  }
  return toMailList;
}

function sendMail(data, template, toList, ccList) {
  let to = toList.join(',')
  let cc = ccList.join(',')

  const tmpSubject = handlebars.compile(template.subject);
  const subjectEmail = tmpSubject(data.toObject());

  const tmpContent = handlebars.compile(template.content);
  const contentEmail = tmpContent(data.toObject());
  return {
    subjectEmail,
    contentEmail,
    to,
    cc
  };
  // sendEmail(subjectEmail, contentEmail, to, cc);
}