import humps from 'humps';
import mongoose from 'mongoose';
import _ from 'lodash';
import pluralize from 'pluralize';

import { flatenModel } from './modelHelper';

const STRING = 'string';
const ARRAY = 'array';
const NUMBER = 'number';
const OBJECT = 'object';
const OBJECTID = 'string'; // UUID
const BOOLEAN = 'boolean';
const DATE = 'string';
const DATE_TIME = 'string';

export const SWAGGER_TYPE = {
  STRING,
  ARRAY,
  NUMBER,
  OBJECT,
  OBJECTID,
  BOOLEAN,
  DATE,
  DATE_TIME
};

export const SWAGGER_TYPE_OBJECT = {
  STRING: {
    type: SWAGGER_TYPE.STRING,
  },

  ARRAY: {
    type: SWAGGER_TYPE.ARRAY,
  },

  NUMBER: {
    type: SWAGGER_TYPE.NUMBER,
  },

  OBJECT: {
    type: SWAGGER_TYPE.OBJECT,
  },

  OBJECTID: {
    type: SWAGGER_TYPE.OBJECTID,
  },

  BOOLEAN: {
    type: SWAGGER_TYPE.BOOLEAN,
  },

  DATE: {
    type: SWAGGER_TYPE.DATE,
    format: 'date',
  },

  DATE_TIME: {
    type: SWAGGER_TYPE.DATE_TIME,
    format: 'date-time',
  },
};

export const AUTH_PARAM = {
  name: 'Authorization',
  in: 'header',
  required: false,
  type: SWAGGER_TYPE.STRING,
};

export const ID_PARAM = {
  name: 'id',
  in: 'path',
  required: true,
  type: SWAGGER_TYPE.STRING,
};

export const LIMIT_PARAM = {
  in: 'query',
  name: 'limit',
  required: false,
  type: SWAGGER_TYPE.NUMBER,
};

export const OFFSET_PARAM = {
  in: 'query',
  name: 'offset',
  required: false,
  type: SWAGGER_TYPE.NUMBER,
};

export const PAYLOAD_MIME_LIST = ['application/json'];

export const BACKGROUND_RESERVERED_FIELD_LIST = [
  'deleteRejectTimeOut',
  'deleted',
  'deletedAt',
  'deletedBy',
  'deletedByUserName',
  'deletedByFullName'
];

export const SYSTEM_RESERVERED_FIELD_LIST = [
  // 'createdAt',
  // 'createdBy',
  // 'createdByUserName',
  // 'createdByFullName',

  // 'updatedAt',
  // 'updatedBy',
  // 'updatedByUserName',
  // 'updatedByFullName',

  'deleteRejectTimeOut',
  'deleted',
  'deletedAt',
  'deletedBy',
  'deletedByUserName',
  'deletedByFullName'
];

export const COMMON_HTTP_ERROR = {
  '404': {
    'description': 'Not Found',
  },

  '422': {
    'description': 'Unprocessable Entity',
  },

  '500': {
    'description': 'Internal Server Error',
  },

  '502': {
    'description': 'Bad Gateway',
  },

  '502': {
    'description': 'Service Unavailable',
  },
}

export const generateGetListResponse = (serviceCode) => (
  {
    ...COMMON_HTTP_ERROR,

    '200': {
      description: 'OK',
      schema: {
        type: SWAGGER_TYPE.OBJECT,
        properties: {
          model: {
            type: SWAGGER_TYPE.STRING,
            description: 'Data model',
          },

          data: {
            type: SWAGGER_TYPE.ARRAY,
            items: {
              $ref: `#/definitions/${serviceCode}`,
            },
            description: 'Array of found paged records',
          },

          length: {
            type: SWAGGER_TYPE.NUMBER,
            description: 'Found record amount',
          },

          query: {
            type: SWAGGER_TYPE.OBJECT,
            description: 'Executed query to find data',
          },
        }
      }
    },
  }
);

export const generateGetByIdResponse = (serviceCode) => (
  {
    ...COMMON_HTTP_ERROR,

    '200': {
      description: 'OK',
      schema: {
        type: SWAGGER_TYPE.OBJECT,
        properties: {
          data: {
            type: SWAGGER_TYPE.OBJECT,
            $ref: `#/definitions/${serviceCode}`,
          },
        }
      }
    },
  }
);

export const generateCreatingResponse = (serviceCode) => (
  {
    ...COMMON_HTTP_ERROR,

    '200': {
      description: 'OK',
    },
  }
);

export const generateUpdatingResponse = (serviceCode) => (
  {
    ...COMMON_HTTP_ERROR,

    '200': {
      description: 'OK',
    },
  }
);

export const generateDeletingResponse = (serviceCode) => (
  {
    ...COMMON_HTTP_ERROR,

    '200': {
      description: 'OK',
    },
  }
);

export const HTTP_PROTOCOL_LIST = ['https', 'http'];

export const generateGetListActionCode = (pascalizedServiceName) => {
  return `get${pascalizedServiceName}List`;
}

export const generateGetByIdActionCode = (pascalizedServiceName) => {
  return `get${pascalizedServiceName}ById`;
}

export const generateCreateActionCode = (pascalizedServiceName) => {
  return `create${pascalizedServiceName}`;
}

export const generateUpdateActionCode = (pascalizedServiceName) => {
  return `update${pascalizedServiceName}ById`;
}

export const generateDeleteActionCode = (pascalizedServiceName) => {
  return `delete${pascalizedServiceName}ById`;
}

export const parseDataModel = (camelizedServiceCode) => {
  const RequestedDataModel = mongoose.model(camelizedServiceCode);

  if (!RequestedDataModel) {
    return null;
  }

  const fieldList = flatenModel(RequestedDataModel.schema.obj);

  const defProps = {
    _id: {
      type: SWAGGER_TYPE.STRING,
    },
  };

  const requiredFields = [];
  const queryParamList = [];

  // queryParamList.push(AUTH_PARAM);
  queryParamList.push(LIMIT_PARAM);
  queryParamList.push(OFFSET_PARAM);

  queryParamList.push({
    name: '_id',
    in: 'query',
    type: SWAGGER_TYPE.STRING,
  });

  Object.entries(fieldList).forEach(([name, value]) => {
    const { type, required } = value;
    let swaggerType = type;

    switch (type) {
      case 'datetime':
      case 'date':
      case 'objectid':
        swaggerType = SWAGGER_TYPE.STRING;
        break;

      default:
        break;
    }

    if (BACKGROUND_RESERVERED_FIELD_LIST.findIndex(f => f === name) < 0) { // skip system auto fields
      queryParamList.push({
        in: 'query',
        name,
        type: swaggerType,
      });
    }

    if (SYSTEM_RESERVERED_FIELD_LIST.findIndex(f => f === name) < 0) { // skip system auto fields
      defProps[name] = {
        type: swaggerType,
      };

      if (required) {
        requiredFields.push(name);
      }
    }
  });

  return {
    queryParamList,
    requiredFields,
    defProps,
  };

}

export const generateSwagger = (camelizedServiceCode, version, description) => {
  const pascalizedServiceName = pluralize.singular(humps.pascalize(camelizedServiceCode));
  const serviceCode = `v${version ? version : '1'}/${camelizedServiceCode}`;
  const serviceName = _.capitalize(humps.depascalize(pascalizedServiceName, { separator: ' ' }));

  const swagger = {};
  const byIdAndBodyParam = [ID_PARAM];
  // byIdAndBodyParam.push(AUTH_PARAM);

  const { queryParamList, requiredFields, defProps } = parseDataModel(camelizedServiceCode);

  byIdAndBodyParam.push({
    in: 'body',
    name: camelizedServiceCode,
    schema: {
      '$ref': `#/definitions/${camelizedServiceCode}`
    }
  });

  swagger.swagger = '2.0';
  swagger.host = 'api.synnexfpt.com';
  swagger.basePath = `/${serviceCode}`;

  swagger.info = {
    title: serviceName,
    version,
    description: description || '',
    contact: {
      email: 'info@synnexfpt.com'
    },
    license: {
      name: 'Apache 2.0',
      url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
    },
  };

  swagger.schemes = HTTP_PROTOCOL_LIST;

  swagger.paths = {
    '/': {
      get: {
        operationId: generateGetListActionCode(pascalizedServiceName),
        summary: `get list`,
        description: '',
        parameters: queryParamList,
        produces: PAYLOAD_MIME_LIST,
        responses: generateGetListResponse(camelizedServiceCode),
      }, // method
    }, // path

    '/:id': {
      get: {
        operationId: generateGetByIdActionCode(pascalizedServiceName),
        summary: `get record by ID`,
        description: '',
        parameters: [ID_PARAM],
        produces: PAYLOAD_MIME_LIST,
        responses: generateGetByIdResponse(camelizedServiceCode),
      }, // method

      post: {
        operationId: generateCreateActionCode(pascalizedServiceName),
        summary: `create new record`,
        description: '',
        parameters: byIdAndBodyParam,
        consumes: PAYLOAD_MIME_LIST,
        produces: PAYLOAD_MIME_LIST,
        responses: generateCreatingResponse(camelizedServiceCode),
      }, // method

      put: {
        operationId: generateUpdateActionCode(pascalizedServiceName),
        summary: `update record`,
        description: '',
        parameters: byIdAndBodyParam,
        consumes: PAYLOAD_MIME_LIST,
        produces: PAYLOAD_MIME_LIST,
        responses: generateUpdatingResponse(camelizedServiceCode),
      }, // method

      delete: {
        operationId: generateDeleteActionCode(pascalizedServiceName),
        summary: `delete record`,
        description: '',
        parameters: [ID_PARAM],
        produces: PAYLOAD_MIME_LIST,
        responses: generateDeletingResponse(camelizedServiceCode),
      }, // method
    }, // path
  }; // paths

  swagger.definitions = {
    [camelizedServiceCode]: {
      type: SWAGGER_TYPE.OBJECT,
      required: requiredFields,
      properties: defProps,
    },
  };

  return swagger;
}
