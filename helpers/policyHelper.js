import _ from 'lodash';
import mongoose from 'mongoose';

import {
  equalToId,
  equalToDate,
  greatThanDate, greatThanOrEqualToDate,
  lessThanDate, lessThanOrEqualToDate,
  equalToDateTime,
  greatThanDateTime, greatThanOrEqualToDateTime,
  lessThanDateTime, lessThanOrEqualToDateTime,
} from './commonHelper';

import { TITLE } from '../constants/titleConstants';
import { USER_FEATURE_LIST, USER_FEATURE } from '../constants/policyConstant';
import { DATA_TYPE, OPERATOR } from '../constants/dataTypeConstant';

export const exactCompanyList = (user) => {
  if (!_.isObject(user)) {
    return [];
  }

  const result = [];
  const { departmentList } = user;

  if (_.isArray(departmentList)) {
    departmentList.forEach((department) => {
      result.push(department.companyId);
    });
  }

  return result;
}

export const exactDepartmentList = (user) => {
  if (!_.isObject(user)) {
    return [];
  }

  const result = [];
  const { departmentList } = user;

  if (_.isArray(departmentList)) {
    departmentList.forEach((department) => {
      result.push(department.departmentId);
    });
  }

  return result;
}


export const exactDepartmentTypeList = (user) => {
  if (!_.isObject(user)) {
    return [];
  }

  const result = [];
  const { departmentList } = user;

  if (_.isArray(departmentList)) {
    departmentList.forEach((department) => {
      result.push(department.departmentTypeId);
    });
  }

  return result;
}

export const exactDivisionList = (user) => {
  if (!_.isObject(user)) {
    return [];
  }

  const result = [];
  const { departmentList } = user;

  if (_.isArray(departmentList)) {
    departmentList.forEach((department) => {
      result.push(department.divisionId);
    });
  }

  return result;
}

export const exactDivisionTypeList = (user) => {
  if (!_.isObject(user)) {
    return [];
  }

  const result = [];
  const { departmentList } = user;

  if (_.isArray(departmentList)) {
    departmentList.forEach((department) => {
      result.push(department.divisionTypeId);
    });
  }

  return result;
}

export const exactRoleList = (user) => {
  if (!_.isObject(user)) {
    return [];
  }

  const result = [];
  const { roleList } = user;

  if (_.isArray(roleList)) {
    roleList.forEach((role) => {
      result.push(role.roleId);
    });
  }

  return result;
}

export const exactProductSegmentList = (user) => {
  if (!_.isObject(user)) {
    return [];
  }

  const result = [];
  const { productSegmentList } = user;

  if (_.isArray(productSegmentList)) {
    productSegmentList.forEach((productSegment) => {
      result.push(productSegment.productSegmentId);
    });
  }

  return result;
}

export const exactVendorList = (user) => {
  if (!_.isObject(user)) {
    return [];
  }

  const result = [];
  const { vendorList } = user;

  if (_.isArray(vendorList)) {
    vendorList.forEach((vendor) => {
      result.push(vendor.vendorId);
    });
  }

  return result;
}

export const exactCustomerList = (user) => {
  if (!_.isObject(user)) {
    return [];
  }

  const result = [];
  const { customerList } = user;

  if (_.isArray(customerList)) {
    customerList.forEach((customer) => {
      result.push(customer.customerId);
    });
  }

  return result;
}

export const exactFunctionList = (user) => {
  if (!_.isObject(user)) {
    return [];
  }

  const result = [];
  const { functionList } = user;

  if (_.isArray(functionList)) {
    functionList.forEach((func) => {
      result.push(func.functionId);
    });
  }

  return result;
}

export const exactStaffList = (user) => {
  if (!_.isObject(user)) {
    return [];
  }

  const result = [];
  // TODO: department.staffList is undefined
  // const { _id, departmentList } = user;

  // if (_.isArray(departmentList)) {
  //   departmentList.forEach((department) => {
  //     const { ceoUserId, departmentManagerUserId, divisionManagerUserId } = department;

  //     if (equalToId(_id, ceoUserId) || equalToId(_id, departmentManagerUserId) || equalToId(_id, divisionManagerUserId)) {
  //       const { staffList } = department
  //       if (_.isArray(staffList)) {
  //         staffList.forEach((staff) => {
  //           result.push(staff.userId);
  //         });
  //       }
  //     }
  //   });
  // }

  return _.uniq(result);
}

export const exactBossList = (user) => {
  if (!_.isObject(user)) {
    return [];
  }

  const result = [];
  const { _id, departmentList } = user;

  if (_.isArray(departmentList)) {
    departmentList.forEach((department) => {
      const { ceoUserId, departmentManagerUserId } = department;

      // if User is:
      //   + CEO => BOSS is himself
      //   + department manager => BOSS is CEO
      //   + division manager => BOSS is Department manager
      //   + staff => BOSS is Department manager [!]

      if (equalToId(_id, ceoUserId) || equalToId(_id, departmentManagerUserId)) {
        result.push(ceoUserId);
      } else {
        result.push(departmentManagerUserId);
      }
    });
  }

  return _.uniq(result);
}

export const exactUserId = (user) => {
  if (!_.isObject(user)) {
    return [];
  }

  const result = [];
  const userId = user.userId ? user.userId : user._id;

  if (userId) {
    result.push(userId);
  }

  return result;
}

export const getUserFeature = (user) => {
  if (!_.isObject(user)) {
    return undefined;
  }

  const userId = user._id;
  const { userName, fullName, email } = user;

  if (!userId || !userName || !fullName) {
    return undefined;
  }

  const userFeatureList = [];

  USER_FEATURE_LIST.forEach((featureName) => {
    const feature = { featureName };
    let found = true;

    switch (featureName) {
      case USER_FEATURE.USER_ID:
        feature.value = [userId];
        break;

      case USER_FEATURE.COMPANY_LIST:
        feature.value = exactCompanyList(user);
        break;

      case USER_FEATURE.DEPARTMENT_LIST:
        feature.value = exactDepartmentList(user);
        break;

      case USER_FEATURE.DEPARTMENT_TYPE_LIST:
          feature.value = exactDepartmentTypeList(user);
          break;

      case USER_FEATURE.DIVISION_LIST:
        feature.value = exactDivisionList(user);
        break;

      case USER_FEATURE.DIVISION_TYPE_LIST:
          feature.value = exactDivisionTypeList(user);
          break;

      case USER_FEATURE.ROLE_LIST:
        feature.value = exactRoleList(user);
        break;

      case USER_FEATURE.FUNCTION_LIST:
        feature.value = exactFunctionList(user);
        break;

      case USER_FEATURE.BOSS_LIST:
        feature.value = exactBossList(user);
        break;

      case USER_FEATURE.STAFF_LIST:
        feature.value = exactStaffList(user);
        break;

      case USER_FEATURE.PRODUCT_SEGMENT_LIST:
        feature.value = exactProductSegmentList(user);
        break;

      case USER_FEATURE.VENDOR_LIST:
        feature.value = exactVendorList(user);
        break;

      case USER_FEATURE.CUSTOMER_LIST:
        feature.value = exactCustomerList(user);
        break;

      default:
        found = false;
        break;
    }

    if (found) {
      userFeatureList.push(feature);
    }
  });

  return {
    userId,
    userName,
    fullName,
    email,
    userFeatureList,
  };
}

export const getUserAccessList = (userFeature, policy) => {
  if (!_.isObject(userFeature) || !_.isObject(policy)) {
    return undefined;
  }

  const relatedPolicyList = [];
  const accessList = [];

  const { userFeatureList } = policy;

  if (userFeatureList.length === 0) { // effect to ALL USER
    relatedPolicyList.push(policy);
  } else {
    let related = true;

    for (let i = 0; i < userFeatureList.length && related; i += 1) {
      const { featureName, selectedOperator, selectedValueList } = userFeatureList[i];
      const feature = userFeature.userFeatureList.find(f => f.featureName === featureName);

      if (feature) {
        switch (selectedOperator) {
          case OPERATOR.IN:
            related = _.intersection(feature.value, selectedValueList).length > 0;
            break;

          case OPERATOR.NOT_IN:
            related = _.intersection(feature.value, selectedValueList).length === 0;
            break;

          case OPERATOR.EQ:
            related = feature.value === selectedValueList;
            break;

          case OPERATOR.NEQ:
            related = feature.value !== selectedValueList;
            break;
        
          default:
            break;
        }
      } else {
        related = false;
      }
    } // for (let i = 0; i < userFeatureList.length && related; i +=1)

    if (related) {
      relatedPolicyList.push(policy);
    }
  }

  relatedPolicyList.forEach(p => {
    accessList.push({
      userId: userFeature.userId,
      userName: userFeature.userName,
      fullName: userFeature.fullName,
      userFeatureList: userFeature.userFeatureList,

      policyId: mongoose.Types.ObjectId(p._id), // data received via Rabbit MQ so Object become Text => have to restore data type
      policyName: p.policyName,

      functionId: mongoose.Types.ObjectId(p.functionId), // data received via Rabbit MQ so Object become Text => have to restore data type
      functionUrl: p.functionUrl,
      functionName: p.functionName,

      serviceId: mongoose.Types.ObjectId(p.serviceId), // data received via Rabbit MQ so Object become Text => have to restore data type
      serviceCode: p.serviceCode,
      serviceName: p.serviceName,

      actionId: mongoose.Types.ObjectId(p.actionId), // data received via Rabbit MQ so Object become Text => have to restore data type
      actionCode: p.actionCode,
      path: p.path,
      method: p.method,
      allowedRequestFieldList: p.allowedRequestFieldList,
      allowedResponseFieldList: p.allowedResponseFieldList,
      recordFeatureList: p.recordFeatureList,
    });
  });

  return accessList;
}

export const checkFieldPermission = (requestFieldList, allowedFieldList) => {
  if (!_.isArray(requestFieldList) || !_.isArray(allowedFieldList)) {
    return false; // TODO: throw exception or NOT True / False value
  }

  if (allowedFieldList.length === 0) {
    return false;
  }

  if (requestFieldList.length === 0) {
    return true;
  }

  return _.difference(requestFieldList, allowedFieldList).length >= 0;
} 

export const checkRecordPermission = (record, userFeatureList, recordFeatureList) => {
  if (!_.isObject(record)) {
    return false; // TODO: throw exception or NOT True / False value
  }

  if (!_.isArray(userFeatureList)) {
    return false; // TODO: throw exception or NOT True / False value
  }

  if (!_.isArray(recordFeatureList)) {
    return false; // TODO: throw exception or NOT True / False value
  }

  if (recordFeatureList.length === 0) {
    return true;
  }

  const allowed = true;
  let i = 0;
  const featureCount = recordFeatureList.length;

  for (i = 0; i < featureCount; i += 1) {
    const {
      featureName, type,
      selectedOperator,
      isUserFeature, selectedValueList,
    } =  recordFeatureList[i];

    const splitFeatureName = featureName.split('.');
    const fieldName = splitFeatureName[0];
    const subFieldName = (splitFeatureName.length > 1) ? splitFeatureName[1] : undefined;

    const fieldValue = record[fieldName];
    let operatedByValue;

    if (isUserFeature) {
      if (!selectedValueList || selectedValueList.length === 0) {
        return false;
      }

      const userFeature = userFeatureList.find(f => f.featureName === selectedValueList);
      
      if (_.isUndefined(userFeature)) {
        return false;
      } else {
        operatedByValue = userFeature.value;
      }
    } else { // if (isUserFeature)
      operatedByValue = selectedValueList;
    } // if (isUserFeature)

    if (_.isUndefined(fieldValue)) {
      return false;
    }

    switch (selectedOperator) {
      case OPERATOR.EQ:
        switch (type) {
          case DATA_TYPE.NUMBER:
            if (fieldValue !== Number(operatedByValue[0])) {
              return false;
            }

            break;

          case DATA_TYPE.DATE:
            if (!equalToDate(fieldValue, new Date(operatedByValue[0]))) {
              return false;
            }

            break;

          case DATA_TYPE.DATE_TIME:
            if (!equalToDateTime(fieldValue, new Date(operatedByValue[0]))) {
              return false;
            }

            break;

          default:
            if (fieldValue.toString() !== operatedByValue.toString()) { // dev test delete [0] of operatedByValue[0]
              return false;
            }

            break;
        } // switch (type)

        break;

      case OPERATOR.NEQ:
        switch (type) {
          case DATA_TYPE.NUMBER:
            if (fieldValue === Number(operatedByValue[0])) {
              return false;
            }

            break;

          case DATA_TYPE.DATE:
            if (equalToDate(fieldValue, new Date(operatedByValue[0]))) {
              return false;
            }

            break;

          case DATA_TYPE.DATE_TIME:
            if (equalToDateTime(fieldValue, new Date(operatedByValue[0]))) {
              return false;
            }

            break;

          default:
            if (fieldValue.toString() === operatedByValue[0].toString()) {
              return false;
            }
            break;
        } // switch (type)

        break;
      
      case OPERATOR.GT:
        switch (type) {
          case DATA_TYPE.NUMBER:
            if (fieldValue <= Number(operatedByValue[0])) {
              return false;
            }

            break;

          case DATA_TYPE.DATE:
            if (!greatThanDate(fieldValue, new Date(operatedByValue[0]))) {
              return false;
            }

            break;

          case DATA_TYPE.DATE_TIME:
            if (!greatThanDateTime(fieldValue, new Date(operatedByValue[0]))) {
              return false;
            }

            break;

          default:
            return false;
        } // switch (type)

        break;
      
      case OPERATOR.GTE:
        switch (type) {
          case DATA_TYPE.NUMBER:
            if (fieldValue < Number(operatedByValue[0])) {
              return false;
            }

            break;
          
          case DATA_TYPE.DATE:
            if (!greatThanOrEqualToDate(fieldValue, new Date(operatedByValue[0]))) {
              return false;
            }

            break;

          case DATA_TYPE.DATE_TIME:
            if (!greatThanOrEqualToDateTime(fieldValue, new Date(operatedByValue[0]))) {
              return false;
            }

            break;

          default:
            return false;
        } // switch (type)

        break;
      
      case OPERATOR.LT:
        switch (type) {
          case DATA_TYPE.NUMBER:
            if (fieldValue >= Number(operatedByValue[0])) {
              return false;
            }

            break;

          case DATA_TYPE.DATE:
            if (!lessThanDate(fieldValue, new Date(operatedByValue[0]))) {
              return false;
            }

            break;

          case DATA_TYPE.DATE_TIME:
            if (!lessThanDateTime(fieldValue, new Date(operatedByValue[0]))) {
              return false;
            }

            break;

          default:
            return false;
        } // switch (type)

        break;
      
      case OPERATOR.LTE:
        switch (type) {
          case DATA_TYPE.NUMBER:
            if (fieldValue > Number(operatedByValue[0])) {
              return false;
            }

            break;

          case DATA_TYPE.DATE:
            if (!lessThanOrEqualToDate(fieldValue, new Date(operatedByValue[0]))) {
              return false;
            }

            break;

          case DATA_TYPE.DATE_TIME:
            if (!lessThanOrEqualToDateTime(fieldValue, new Date(operatedByValue[0]))) {
              return false;
            }

            break;

          default:
            return false;
        } // switch (type)

        break;
      
      case OPERATOR.IN:
        if (subFieldName) { // nested document
          const fieldValueList = [];

          fieldValue.forEach((e) => {
            fieldValueList.push(e[subFieldName].toString());
          });

          if (_.difference(fieldValueList, operatedByValue).length > 0) {
            return false;
          }
        } else {
          switch (type) {
            case DATA_TYPE.ARRAY: {
              if (_.difference(fieldValue, operatedByValue).length > 0) {
                return false;
              }
  
              break;
            }
  
            default: {
              // [!] cast to string to commpare not string value
              if (_.difference([fieldValue.toString()], operatedByValue).length > 0) {
                return false;
              }
  
              break;
            }
          } // switch (type)
        }

        break;

      case OPERATOR.NOT_IN:
        switch (type) {
          case DATA_TYPE.ARRAY:
            if (_.intersection(fieldValue, operatedByValue).length > 0) {
              return false;
            }

            break;

          default:
            if (_.intersection([fieldValue], operatedByValue).length > 0) {
              return false;
            }

            break;
        } // switch (type)

        break;

    } // switch (selectedOperator)
  } // for (i = 0; i < featureCount; i += 1)

  return (i >= featureCount); // check all field

}
