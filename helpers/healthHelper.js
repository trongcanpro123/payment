import mongoose from 'mongoose';

const healthcheck = (req, res) => { 
  res.status(200).json({
    status: "Hi! I'm good!",
    // esb: app.locals.esb,
    // mongoose: mongoose.connection,
  });
};

export default healthcheck;