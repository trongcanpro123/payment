import jwt from 'jsonwebtoken';
import fs from 'fs';
import path from 'path';

module.exports = function (req, res, next) {
  const reqHeaders = req.headers;
  const authHeader = reqHeaders.authorization ? reqHeaders.authorization : '';
  const splittedAuthHeader = authHeader ? authHeader.split(' ') : [];
  const token = (splittedAuthHeader[0] === 'Bearer') ? splittedAuthHeader[1] : '';
  const functionId = reqHeaders['x-function-id'] ? reqHeaders['x-function-id'] : '';

  if (token) {
    const cert = fs.readFileSync(path.resolve(__dirname, '../certs/token.public.pem'));

    jwt.verify(token, cert, function(err, jwtPayload) {
      if (err) {
        res.redirect(`${reqHeaders.origin}/logout`);
      } else {
        req.user = {
          userId: jwtPayload['userId'] ? jwtPayload['userId'] : '',
          userName: jwtPayload['userName'] ? jwtPayload['userName'] : '',
          fullName: jwtPayload['fullName'] ? jwtPayload['fullName'] : '',
          isAdmin: jwtPayload['isAdmin'] ? jwtPayload['isAdmin'] : '',

          functionId,
        };
      }

      next();
    });
  } else {
    req.user = {};
    next();
  }
}
