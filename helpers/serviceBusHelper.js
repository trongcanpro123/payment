import debug from 'debug';
import async from 'async';

import { app } from '../server';
import { createEvent, nomalizeModelName } from './eventHelper';
import commonEvent from '../constants/eventConstant';
import { getEvent } from '../helpers/eventHelper';
import { validateObjectId } from '../helpers/commonHelper';

const amqp = require('amqplib/callback_api');
const serviceBusDebugger = debug('app:sb');
const uuidv4 = require('uuid/v4');

let sb = {};

const connectToMessageBroker = (esb, cb) => {
  amqp.connect(esb.messageBrokerUri, (err, conn) => {
    if (err) {
      serviceBusDebugger(`[SB] Connect to RabbitMQ error: ${err}`);
      cb(err);
    } else {
      esb.connection = conn;
      cb(null, esb);
    }
  });
};

const initExchange = (esb, cb) => {
  esb.connection.createChannel((err, channel) => {
    if (err) {
      serviceBusDebugger(`[SB] Create RabbitMQ channel error: ${err}`);
      cb(err);
    } else {
      channel.prefetch(1);
      channel.assertExchange(esb.exchangeName, 'direct', { durable: true });

      esb.channel = channel;

      cb(null, esb);  
    }
  });
};

sb.init = (messageBrokerUri, exchangeName, cb) => {
  const esb = {};

  esb.messageBrokerUri = messageBrokerUri;
  esb.exchangeName = exchangeName;
  esb.connection = null;
  esb.channel = null;

  async.waterfall([
    async.apply(connectToMessageBroker, esb),
    initExchange,
  ], (err, serviceBus) => {
      if (err) {
        cb(err);
      } else {
        app.locals.esb = serviceBus;
        cb(null)
      }
  });
};

sb.consume = (modelName, servedEvent, eventHandler, cb) => {
  const { esb } = app.locals;
  const queueName = nomalizeModelName(modelName);

  esb.channel.assertQueue(queueName, { durable: true }, (err, mq) => {
    const { exchangeName } = esb;
    const { queue } = mq;

    if (err) {
      serviceBusDebugger(`[SB] Cannot assert queue [${targetService}] with error: \n ${err}.`);
      cb(err);
    } else {
      servedEvent.forEach((event) => {
        serviceBusDebugger(`[SB] Binding event "${event}" => queue [${queueName}].`);
        esb.channel.bindQueue(queue, exchangeName, event);
      });

      esb.channel.consume(queue, (msg) => {
        if (msg !== null) {
          const { properties, content } = msg;
          const { replyTo, type, correlationId } = properties;
          const msgContent = content.toString();

          serviceBusDebugger(`[SB] handling event "${type}" with payload:\n ${msgContent} }`);
    
          if (servedEvent.length > 0) {
            const event = createEvent(replyTo, type, JSON.parse(msgContent), correlationId);

            eventHandler(
              event,
              (err) => {
                if (!err) {
                  serviceBusDebugger(`[SB] handled event "${type}" with payload:\n ${msgContent} }`);
                } else {
                  serviceBusDebugger(`[SB] cannot handle event "${type}" with payload:\n ${msgContent} } \n throw error:\n ${err}.`);
                }

                // if (modelName.indexOf('serviceBusEvents') < 0) {
                //   const eventType = err ? getEvent(modelName, commonEvent.CAN_NOT_HANDLED) : getEvent(queueName, commonEvent.HANDLED);

                //   sb.publish(
                //     queueName,
                //     eventType,
                //     { error: err || null },
                //     correlationId,
                //     (err) => {
                //       if (err) {
                //         serviceBusDebugger(`[SB] push to ServiceBusEvent get error:\n ${colors.red(err)}`);
                //       }
                //     },
                //   );
                // }

                esb.channel.ack(msg);
              }
            );
          } else {
            serviceBusDebugger(`[SB] skipped handle event "${type}" with payload: \n ${msgContent}.`);
    
            esb.channel.ack(msg);
          }
        } else {
          esb.channel.reject(msg);
        }
      });
    
      cb(null);
    }
  });
};

sb.publish = (origin, type, payload, transactionId, cb) => {
  const { esb } = app.locals;
  const correlationId = validateObjectId(transactionId) ? transactionId : uuidv4();

  try {
    const msgContent = JSON.stringify(payload);

    const msgOptions = {
      contentType: 'application/json',
      correlationId,
      replyTo: origin,
      type
    };

    esb.channel.publish(esb.exchangeName, type, Buffer.from(msgContent), msgOptions);

    serviceBusDebugger(`[SB] ${origin} triggered event "${type}" with content:\n ${msgContent}.`);

    if (cb) {
      cb(null);
    }
  } catch (error) {
    serviceBusDebugger(`[SB] ${origin}] cannot trigger event [${type}] with error:\n ${error}.`);

    if (cb) {
      cb(error);
    }
  }
};

export default sb;