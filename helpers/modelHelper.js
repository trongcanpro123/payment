
import _ from 'underscore';

export const getFieldTypeName = (fieldValue) => {
  const type = (fieldValue.type ? fieldValue.type.name : fieldValue.name)
  return type ? type.toLowerCase() : undefined;
}

export const flatenModel = (model) => {
  const fieldList = {};

  Object.entries(model).forEach(([fieldName, fieldValue]) => {
    if (!fieldValue) {
    }

    if (!_.isArray(fieldValue)) {
      fieldList[fieldName] = fieldValue.enum ? {
        type: getFieldTypeName(fieldValue),
        required: fieldValue.required || false,
        enum: fieldValue.enum || undefined,
      } : {
        type: getFieldTypeName(fieldValue),
        required: fieldValue.required || false,
      };
    } else {
      const nestedDocument = fieldValue[0];

      Object.entries(nestedDocument).forEach(([subFieldName, subFieldValue]) => {
        if (!_.isArray(subFieldValue)) {
          fieldList[`${fieldName}.${subFieldName}`] = fieldValue.enum ? {
            type: getFieldTypeName(subFieldValue),
            required: subFieldValue.required || false,
            enum: fieldValue.enum || undefined,
          } : {
            type: getFieldTypeName(subFieldValue),
            required: subFieldValue.required || false,
          };
        } else {
          const nestedDocument2 = subFieldValue[0];

          Object.entries(nestedDocument2).forEach(([subFieldName2, subFieldValue2]) => {
            fieldList[`${fieldName}.${subFieldName}.${subFieldName2}`] = subFieldValue2.enum ? {
              type: getFieldTypeName(subFieldValue2),
              required: subFieldValue2.required || false,
              enum: subFieldValue2.enum || undefined
            } : {
              type: getFieldTypeName(subFieldValue2),
              required: subFieldValue2.required || false,
            };
          });
        }
      }) // Object.entries(nestedDocument).forEach(..)
    }
  }); // Object.entries(model).forEach(...)

  return fieldList;
}

export const getDbModelName = (modelName) => {
  return modelName.substring(modelName.indexOf('/') + 1)
}