import mongoose from 'mongoose';
import async from 'async';
import orderEvent from '../events/orderEvent';
import sb from '../../helpers/serviceBusHelper';
import debug from 'debug';
const serviceBusDebugger = debug('app:serviceBus');
import { getEvent } from '../../helpers/eventHelper';
import { commonEvent } from '../../constants/eventConstant';
export const modelOrder = 'eOrders';
export const model = 'payment';

export const servedEvent = [
  orderEvent.CREATED,
];

export const handle = async (event, eventHandleCallBack) => {
  try {
    const DataModel = mongoose.model(model);
    const orderModel = mongoose.model('payment');
    const taskList = [];

    switch (event.type) {
      case orderEvent.CREATED: {
        const data = event.payload.newData;
        // console.log('data', data);
        const dataObject = new DataModel();
        await orderModel.updateMany({ orderNumber: data.orderNumber }, { "$set": { orderState: "confirmed" } })

        Object.entries(data).forEach(([key, value]) => {
          if (key !== '_id') {
            // console.log('key', key);
            dataObject[key] = value;
            // console.log('dataObject', dataObject);
          }
        });
        dataObject.save().then((data2) => {
          // const eventType = getEvent(model, commonEvent.CREATED_BY_API);
          const eventType = getEvent(model, commonEvent.CREATED_BY_API_V2);

          sb.publish(model, eventType, { model, oldData: {}, newData: data2 }, '0', (err) => {
            if (err) {
              serviceBusDebugger(`[SB] Publish error: ${colors.red(err)}`);
            }
          });
        });

        break;
      }
      default: {
        break;
      }
    } // switch (event.type)

    if (taskList.length > 0) {
      async.series(taskList, eventHandleCallBack);
    } else {
      eventHandleCallBack(null);
    }
  } catch (error) {
    eventHandleCallBack(error);
  }
};
