const esdProductEventHandler = require('./esdProductEventHandler');
const esdSalesteamEventHandler = require('./esdSalesteamEventHandler');
const esdOrderEventHandler = require('./esdOrderEventHandler');
const esdCustomerEventHandler = require('./esdCustomerEventHandler');

const eventHandlerList = [];

eventHandlerList.push(esdProductEventHandler);
eventHandlerList.push(esdSalesteamEventHandler);
eventHandlerList.push(esdOrderEventHandler);
eventHandlerList.push(esdCustomerEventHandler);
export default eventHandlerList;
