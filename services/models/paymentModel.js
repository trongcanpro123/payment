const mongoose = require('mongoose');
const esdOrderSchema = new mongoose.Schema({
  orderNumber: { type: String, unique: true },
  orderStatus: { type: String },

  active: { type: Boolean, required: true, default: true },

  createdAt: { type: Date },
  createdBy: { type: mongoose.Schema.Types.ObjectId },
  createdByUserName: String,
  createdByFullName: String,
  createdByEmail: String,
  createdByPhone: String,

  updatedAt: Date,
  updatedBy: { type: mongoose.Schema.Types.ObjectId },
  updatedByUserName: String,
  updatedByFullName: String,

  deleteInProcess: { type: Boolean, default: false },
  deleteRejectTimeOut: { type: Date },
  deleted: { type: Boolean, default: false },
  deletedAt: Date,
  deletedBy: { type: mongoose.Schema.Types.ObjectId },
  deletedByUserName: String,
  deletedByFullName: String,
}, { timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' } });


esdOrderSchema.pre('save', async function (next) {
  try {
    next();
  } catch (error) {
    console.log('error', error);
    next(error);
  }
});

mongoose.model('payments', esdOrderSchema, 'payments');
