import {
  ID_PARAM, SWAGGER_TYPE_OBJECT, PAYLOAD_MIME_LIST,
  generateSwagger, generateUpdatingResponse,
} from '../../helpers/swaggerHelper';

import { REPAIR_STATE_LIST } from '../constants/serviceOrderConstant';
import { SPARE_PART_STATE_LIST } from '../constants/sparePartConstant';

const modelName = 'serviceOrders';
const version = '1';

const swagger = generateSwagger(modelName, version);
const responses = generateUpdatingResponse(`v${version}/${modelName}`);

swagger.paths['/repair/:id'] = {};

swagger.paths['/repair/:id'].put = {
  operationId: 'updateRepairTicketById',
  summary: 'Update repair ticket by Id',
  description: '',
  parameters: [
    ID_PARAM,
    {
      in: 'body',
      name: 'repairTicket',
      schema: {
        '$ref': '#/definitions/RepairTicket'
      },
    },
  ],
  consumes: PAYLOAD_MIME_LIST,
  produces: PAYLOAD_MIME_LIST,
  responses,
};

swagger.definitions.RepairTicket = {
  type: 'object',
  required: [
    'technicianStoreId',
    'technicianStoreCode',
    'technicianStoreName',

    'symptomList',
    'repairCodeList',
    'sparePartList',

    'isCid',
  ],
  properties: {
    repairState: {
      ...SWAGGER_TYPE_OBJECT.STRING,
      enum: REPAIR_STATE_LIST,
    },

    technicianStoreId: SWAGGER_TYPE_OBJECT.OBJECTID,
    technicianStoreCode: SWAGGER_TYPE_OBJECT.STRING,
    technicianStoreName: SWAGGER_TYPE_OBJECT.STRING,

    technicianStoreTypeId: SWAGGER_TYPE_OBJECT.OBJECTID,
    technicianStoreTypeCode: SWAGGER_TYPE_OBJECT.STRING,
    technicianStoreTypeName: SWAGGER_TYPE_OBJECT.STRING,

    quotationState: SWAGGER_TYPE_OBJECT.STRING,
    quotationTotal: SWAGGER_TYPE_OBJECT.NUMBER,
    quotationNote: SWAGGER_TYPE_OBJECT.STRING,

    symptomList: {
      ...SWAGGER_TYPE_OBJECT.ARRAY,
      items: {
        $ref: '#/definitions/Symptom',
      }
    },

    repairCodeList: {
      ...SWAGGER_TYPE_OBJECT.ARRAY,
      items: {
        $ref: '#/definitions/RepairCode',
      }
    },

    sparePartList: {
      ...SWAGGER_TYPE_OBJECT.ARRAY,
      items: {
        $ref: '#/definitions/UsedSparePart',
      }
    },

    isCid: SWAGGER_TYPE_OBJECT.BOOLEAN,
    repairNote: SWAGGER_TYPE_OBJECT.STRING,
  },
};

swagger.definitions.Symptom = {
  type: 'object',
  required: [
    'symptomId',
    'symptomCode',
    'symptomName',
  ],

  properties: {
    symptomId: SWAGGER_TYPE_OBJECT.OBJECTID,
    symptomCode: SWAGGER_TYPE_OBJECT.OBJECTID,
    symptomName: SWAGGER_TYPE_OBJECT.OBJECTID,
  },
};

swagger.definitions.RepairCode = {
  type: 'object',
  required: [
    'repairCodeId',
    'repairCode',
    'repairName',
  ],

  properties: {
    repairCodeId: SWAGGER_TYPE_OBJECT.OBJECTID,
    repairCode: SWAGGER_TYPE_OBJECT.STRING,
    repairName: SWAGGER_TYPE_OBJECT.STRING,
  },
};

swagger.definitions.UsedSparePart = {
  type: 'object',
  required: [
    'goodStoreId',
    'goodStoreCode',
    'goodStoreName',

    'goodStoreTypeId',
    'goodStoreTypeCode',
    'goodStoreTypeName',

    'productId',
    'productCode',
    'productName',

    'productLineId',
    'productLineCode',
    'productLineName',

    'producerId',
    'producerCode',
    'producerName',

    'qty',
    'sparePartState'
  ],

  properties: {
    goodStoreId: SWAGGER_TYPE_OBJECT.OBJECTID,
    goodStoreCode: SWAGGER_TYPE_OBJECT.STRING,
    goodStoreName: SWAGGER_TYPE_OBJECT.STRING,

    goodStoreTypeId: SWAGGER_TYPE_OBJECT.OBJECTID,
    goodStoreTypeCode: SWAGGER_TYPE_OBJECT.STRING,
    goodStoreTypeName: SWAGGER_TYPE_OBJECT.STRING,

    productId: SWAGGER_TYPE_OBJECT.OBJECTID,
    productCode: SWAGGER_TYPE_OBJECT.STRING,
    productName: SWAGGER_TYPE_OBJECT.STRING,

    productLineId: SWAGGER_TYPE_OBJECT.OBJECTID,
    productLineCode: SWAGGER_TYPE_OBJECT.STRING,
    productLineName: SWAGGER_TYPE_OBJECT.STRING,

    producerId: SWAGGER_TYPE_OBJECT.OBJECTID,
    producerCode: SWAGGER_TYPE_OBJECT.STRING,
    producerName: SWAGGER_TYPE_OBJECT.STRING,

    qty: SWAGGER_TYPE_OBJECT.NUMBER,

    sparePartState: {
      ...SWAGGER_TYPE_OBJECT.STRING,
      enum: SPARE_PART_STATE_LIST,
    },

    originSerial: SWAGGER_TYPE_OBJECT.STRING,
    replacedSerial: SWAGGER_TYPE_OBJECT.STRING,
  },
};

swagger.paths['/finalTest/:id'] = {};

swagger.paths['/finalTest/:id'].put = {
  operationId: 'updateFinalTestById',
  summary: 'Update final test by Id',
  description: '',
  parameters: [
    ID_PARAM,
    {
      in: 'body',
      name: 'finalTest',
      schema: {
        '$ref': '#/definitions/FinalTest'
      },
    },
  ],
  consumes: PAYLOAD_MIME_LIST,
  produces: PAYLOAD_MIME_LIST,
  responses,
};

swagger.definitions.FinalTest = {
  type: 'object',

  required: [
    'finalTestState',
    'finalTestNote',
  ],

  properties: {
    finalTestState: SWAGGER_TYPE_OBJECT.STRING,
    finalTestNote: SWAGGER_TYPE_OBJECT.STRING,
  },
};

swagger.paths['/return/:id'] = {};

swagger.paths['/return/:id'].put = {
  operationId: 'updateReturnTicketById',
  summary: 'Update return ticket by Id',
  description: '',
  parameters: [
    ID_PARAM,
    {
      in: 'body',
      name: 'returnedNote',
      schema: {
        '$ref': '#/definitions/ReturnTicket'
      },
    },
  ],
  consumes: PAYLOAD_MIME_LIST,
  produces: PAYLOAD_MIME_LIST,
  responses,
};

swagger.definitions.ReturnTicket = {
  type: 'object',

  required: [
    'returnedNote',
  ],

  properties: {
    returnedNote: SWAGGER_TYPE_OBJECT.STRING,
  },
};

export default swagger;
