const router = require('express').Router();

const autoCrudModel = [
  '/v2/payment',
];
router.use('/v2/uuids', require('./uuidController'));
router.use(autoCrudModel, require('./crudController'));

module.exports = router;
