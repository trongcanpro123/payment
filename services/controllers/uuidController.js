import mongoose from 'mongoose';
import healthcheck from '../../helpers/healthHelper';

const router = require('express').Router();

router.get('/healthcheck', healthcheck);

router.get('/', async (req, res) => {
  const { model } = req.query;
  const Uuid = mongoose.model('uuids');
  const dataObject = new Uuid();

  if (typeof model !== 'string') {
    return res.status(422);
  }

  dataObject.model = model;
  dataObject.createdBy = req.user.userId;
  dataObject.createdByUserName = req.user.userName;
  dataObject.createdByFullName = req.user.fullName;

  return dataObject.save().then((data) => {
    if (data) {
      return res.status(200).json({ data: data._id });
    }

    return res.status(422);
  }).catch(error => res.status(500).json(error));
});

module.exports = router;
